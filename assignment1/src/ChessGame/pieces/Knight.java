package ChessGame.pieces;

import ChessGame.Chessboard;
import ChessGame.Utils;

public class Knight extends Piece {
    /**
     * Constructor
     * @param color The color of the piece
     */
    public Knight(int color){
        this.setColor(color);
        this.setType(Piece.KNIGHT);
        this.setIsAlive(true);
        if (color == Piece.BLACK){this.setSymbol('♞');}
        else{this.setSymbol('♘');}
    }

    /**
     * Return true if it's valid to move to the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard where all ChessGame.pieces move
     * @return Return true if it's valid to move to the target position
     */
    @Override
    public boolean canMove(int x, int y, Chessboard chessboard) {
        if(chessboard.getPieceByPosition(x,y)!=null &&chessboard.getPieceByPosition(x, y).getColor() == this.getColor())return false;

        int[] xy = chessboard.getCoordinatesByPiece(this);
        if(Utils.isLShape(xy[0], xy[1],x, y)){
            return true;
        }
        return false;
    }
}
