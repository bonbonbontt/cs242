package ChessGame.pieces;

import ChessGame.Chessboard;
import ChessGame.Utils;

public class King extends Piece {
    /**
     * Constructor
     * @param color The color of the piece
     */
    public King(int color){
        this.setColor(color);
        this.setType(Piece.KING);
        this.setIsAlive(true);
        if (color == Piece.BLACK){this.setSymbol('♚');}
        else{this.setSymbol('♔');}
    }

    /**
     * Return true if it's valid to move to the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard
     * @return  Return true if it's valid to move to the target position
     */
    @Override
    public boolean canMove(int x, int y, Chessboard chessboard){
        if(chessboard.getPieceByPosition(x,y)!=null &&chessboard.getPieceByPosition(x, y).getColor() == this.getColor())return false;

        int[] xy = chessboard.getCoordinatesByPiece(this);
        boolean move = Utils.isInOneSquare(xy[0], xy[1],x, y) && chessboard.getPieceByPosition(x, y) == null;
        boolean kill = chessboard.getPieceByPosition(x, y) != null
                && Utils.isInOneSquare(xy[0], xy[1],x, y)
                && !chessboard.getPieceByPosition(x, y).isKing()
                && chessboard.getPieceByPosition(x,y).getColor()!=this.getColor();
        if(move || kill){
            return true;
        }
        return false;
    }

}



