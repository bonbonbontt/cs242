package ChessGame.pieces;

import ChessGame.Chessboard;
import ChessGame.Utils;

public class Queen extends Piece {
    /**
     * Constructor
     * @param color The color of the piece
     */
    public Queen(int color){
        this.setIsAlive(true);
        this.setColor(color);
        this.setType(Piece.QUEEN);
        if (color == Piece.BLACK){this.setSymbol('♛');}
        else{this.setSymbol('♕');}
    }

    /**
     * Return true if it's valid to move to the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard where all ChessGame.pieces move
     * @return Return true if it's valid to move to the target position
     */
    @Override
    public boolean canMove(int x, int y, Chessboard chessboard) {
        if(chessboard.getPieceByPosition(x,y)!=null &&chessboard.getPieceByPosition(x, y).getColor() == this.getColor())return false;

        int[] xy = chessboard.getCoordinatesByPiece(this);
        if((Utils.isSameFile(xy[0], x) && !Utils.hasChessOnTheWay(xy[0], xy[1], x, y, chessboard)) ||
                (Utils.isSameRank(xy[1], y) && !Utils.hasChessOnTheWay(xy[0], xy[1], x, y, chessboard))){
            return true;
        }
        if(Utils.isDiagonally(xy[0], xy[1], x, y) && !Utils.hasChessOnTheWayDiagonally(xy[0], xy[1], x, y, chessboard)) {
            return true;
        }
        return false;
    }
}
