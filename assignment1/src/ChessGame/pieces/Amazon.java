package ChessGame.pieces;

import ChessGame.Chessboard;
import ChessGame.Utils;

public class Amazon extends Piece {
    /**
     * Constructor
     * @param color The color of the piece
     */
    public Amazon(int color){
        this.setColor(color);
        this.setType(Piece.AMAZON);
        this.setIsAlive(true);
        if (color == Piece.BLACK){this.setSymbol('⚉');}
        else{this.setSymbol('⚇');}
    }

    /**
     * only move along main diagonal and move to adjacent square along diagonal
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard w\here all ChessGame.pieces move
     * @return Return true if it's valid to move to the target position
     */
    public boolean canMove(int x, int y, Chessboard chessboard){
        if(chessboard.getPieceByPosition(x,y)!=null &&chessboard.getPieceByPosition(x, y).getColor() == this.getColor())return false;
        int[] xy = chessboard.getCoordinatesByPiece(this);
        boolean move = chessboard.isUnoccupied(x, y) && (x == y) && (xy[0] == xy[1]);
        boolean kill =(Math.abs(x - xy[0]) != 1 ||  Math.abs(y - xy[1]) != 1)
                && Utils.isInOneSquare(xy[0], xy[1], x, y);
        if(move || kill){
            return true;
        }
        return false;
    }
}
