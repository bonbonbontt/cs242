package ChessGame;

import view.Controller;
import view.View;


public class main {
    public static void main(String[] args){
        //setup
        Chessboard chessboard = new Chessboard(8, 8);
        chessboard.setTheBoard();
        chessboard.setCurrentPlayerColor(Player.WHITE); // set white
        View view = new View();
        Controller controller = new Controller(chessboard, view);

        //initialize
        chessboard.setTheBoard();
        view.initializeIcon();

        //input name
        while(chessboard.getPlayer1().getName() == null || chessboard.getPlayer2().getName() == null){
            if(chessboard.getPlayer1().getName()!= null)break;
            System.out.println("1");
        }
        System.out.println("out");
        controller.addListeners();

    }//end of main
}//end of class
