package ChessGame.tests;

import ChessGame.pieces.Piece;
import ChessGame.Chessboard;
import ChessGame.pieces.Knight;
import junit.framework.TestCase;

import static ChessGame.pieces.Piece.BLACK;
import static ChessGame.pieces.Piece.WHITE;

public class KnightTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    //Test constructor
    public void testConstructor() {
        chessboard.setTheBoard();
        Piece piece = new Knight(BLACK);
        assertEquals(BLACK, piece.getColor());
        assertEquals(Piece.KNIGHT, piece.getType());
        assertEquals('♞', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }

    public void testLShape(){
        Piece piece = new Knight(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(5 , 2, chessboard));
        assertEquals(true, piece.canMove(5 , 4, chessboard));
        assertEquals(true, piece.canMove(4 , 5, chessboard));
        assertEquals(true, piece.canMove(1 , 2, chessboard));
    }

    public void testNotLShape(){
        Piece piece = new Knight(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(false, piece.canMove(3 , 2, chessboard));
        assertEquals(false, piece.canMove(4 , 4, chessboard));
    }
}