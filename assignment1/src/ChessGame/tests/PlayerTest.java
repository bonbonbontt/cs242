package ChessGame.tests;

import ChessGame.Chessboard;
import ChessGame.Player;
import junit.framework.TestCase;

public class PlayerTest extends TestCase {
    Chessboard chessboard = new Chessboard(8, 8);

    public void testGetColor() throws Exception {
        Player p = new Player(Player.WHITE);
        assertEquals(Player.WHITE, p.getColor());
    }

    public void testGetNumberOfAlivePieces() throws Exception {
        Player p = new Player(Player.WHITE);
        assertEquals(16, p.getNumberOfAlivePieces());

        p.decreaseNumberOfPieces();
        assertEquals(15, p.getNumberOfAlivePieces());
    }

    public void testGetName() throws Exception {
        Player p = new Player(Player.WHITE);
        p.setName("alice");
        assertEquals("alice", p.getName());
    }


    public void testGetScore() throws Exception {
        Player p = new Player(Player.WHITE);
        p.setScore(10);
        assertEquals(10, p.getScore());

        p.addScore(10);
        assertEquals(20, p.getScore());
        p.minusScore(10);
        assertEquals(10, p.getScore());
    }



}