package ChessGame.tests;

import ChessGame.*;
import junit.framework.TestCase;
import ChessGame.pieces.Hopper;
import ChessGame.pieces.King;
import ChessGame.pieces.Pawn;
import ChessGame.pieces.Piece;

import static ChessGame.pieces.Piece.WHITE;

public class HopperTest extends TestCase {
    Chessboard chessboard = new Chessboard(8, 8);

    //Test constructor
    public void testConstructor() {
        Piece piece = new Hopper(WHITE);
        assertEquals(WHITE, piece.getColor());
        assertEquals(Piece.HOPPER, piece.getType());
        assertEquals('☖', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }

    public void testCanJumpAlongFile(){
        Piece piece = new Hopper(Piece.WHITE);
        Piece king = new King(Piece.WHITE);
        Piece p = new Pawn(Piece.WHITE);
        Piece p2 = new Pawn(Piece.WHITE);
        //from top to bottom
        chessboard.setBoard(3, 0, piece);
        chessboard.setBoard(3, 1, king);
        chessboard.setBoard(3, 3, p);
        chessboard.setBoard(3, 5, p2);
        assertEquals(true, Utils.canJump(3, 0, 3, 6, chessboard));
        //from bottom to top
        chessboard.setBoard(3, 0, piece);
        assertEquals(true, Utils.canJump(3, 0, 3, 6, chessboard));
    }

    public void testCanJumpAlongRank(){
        Piece piece = new Hopper(Piece.WHITE);
        Piece king = new King(Piece.WHITE);
        Piece p = new Pawn(Piece.WHITE);
        Piece p2 = new Pawn(Piece.WHITE);

        //from left to right
        chessboard.setBoard(0, 0, piece);
        chessboard.setBoard(1, 0, king);
        chessboard.setBoard(3, 0, p);
        chessboard.setBoard(5, 0, p2);
        assertEquals(true, Utils.canJump(0, 0, 6, 0, chessboard));

        //from right to left
        chessboard.setBoard(6, 0, piece);
        chessboard.setBoard(0, 0, null);
        assertEquals(true, Utils.canJump(6, 0, 0, 0, chessboard));
    }

    public void testMoveFormard(){
        Piece piece = new Hopper(Piece.WHITE);
        chessboard.setBoard(7, 7, piece);
        Piece king = new King(Piece.BLACK);
        chessboard.setBoard(7, 6, king);
        assertEquals(true, piece.canMove(7, 6, chessboard));
        assertEquals(false, piece.canMove(7, 0, chessboard));
    }

}