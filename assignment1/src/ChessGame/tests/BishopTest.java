package ChessGame.tests;

import ChessGame.*;
import junit.framework.TestCase;
import ChessGame.pieces.Bishop;
import ChessGame.pieces.Piece;

import static ChessGame.pieces.Piece.WHITE;

public class BishopTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    //Test constructor
    public void testConstructor() {
        chessboard.setTheBoard();
        Piece piece = new Bishop(WHITE);
        assertEquals(WHITE, piece.getColor());
        assertEquals(Piece.BISHOP, piece.getType());
        assertEquals('♗', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }

    public void testMoveTopLeft() {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(2, 2, chessboard));
    }

    public void testMoveTopRight() {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(4, 2, chessboard));
    }

    public void testMoveBottomLeft() {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(2, 4, chessboard));
    }

    public void testMoveBottomRight() {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(5, 5, chessboard));
    }

    public void testMoveNotDiagonally() {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(false, piece.canMove(3, 5, chessboard));
    }

    public void testLeapOver() {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(3, 3, piece);
        Piece piece1 = new Bishop(WHITE);
        chessboard.setBoard(2, 4, piece1);
        assertEquals(false, piece.canMove(1, 5, chessboard));
    }
}