package ChessGame.tests;

import ChessGame.pieces.Piece;
import ChessGame.Chessboard;
import ChessGame.pieces.Rook;
import junit.framework.TestCase;

import static ChessGame.pieces.Piece.BLACK;
import static ChessGame.pieces.Piece.WHITE;

public class RookTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    public void testConstructor() {
        chessboard.setTheBoard();
        Piece piece = new Rook(BLACK);
        assertEquals(BLACK, piece.getColor());
        assertEquals(Piece.ROOK, piece.getType());
        assertEquals('♜', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }

    public void testMoveAlongRank(){
        Piece piece = new Rook(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(0 , 3, chessboard));
    }

    public void testMoveAlongFile() {
        Piece piece = new Rook(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(3 , 5, chessboard));
    }

    public void testMoveDiagonally() {
        Piece piece = new Rook(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(false, piece.canMove(4 , 4, chessboard));
    }

    public void testLeapOver() {
        Piece piece = new Rook(WHITE);
        chessboard.setBoard(3, 3, piece);
        Piece piece1 = new Rook(WHITE);
        chessboard.setBoard(3, 4, piece1);
        assertEquals(false, piece.canMove(3 , 5, chessboard));
    }
}