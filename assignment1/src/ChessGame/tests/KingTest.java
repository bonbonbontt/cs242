package ChessGame.tests;

import ChessGame.pieces.Piece;
import ChessGame.Chessboard;
import ChessGame.pieces.King;
import junit.framework.TestCase;

import static ChessGame.pieces.Piece.BLACK;
import static ChessGame.pieces.Piece.WHITE;

public class KingTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    public void testConstructor() {
        Piece piece = new King(BLACK);
        assertEquals(BLACK, piece.getColor());
        assertEquals(Piece.KING, piece.getType());
        assertEquals('♚', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }

    public void testMoveAlongFile(){
        Piece king = new King(WHITE);
        chessboard.setBoard(3, 3, king);
        assertEquals(true, king.canMove(3 , 4, chessboard));
    }

    public void testMoveAlongRank(){
        Piece king = new King(WHITE);
        chessboard.setBoard(3, 3, king);
        assertEquals(true, king.canMove(2 , 3, chessboard));
    }

    public void testMoveOutOfOneSqaure(){
        Piece king = new King(WHITE);
        chessboard.setBoard(3, 3, king);
        assertEquals(false, king.canMove(1 , 3, chessboard));
    }

    public void testMoveDiagonally(){
        Piece king = new King(WHITE);
        chessboard.setBoard(3, 3, king);
        assertEquals(true, king.canMove(4 , 4, chessboard));
    }
}