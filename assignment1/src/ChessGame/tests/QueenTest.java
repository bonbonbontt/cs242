package ChessGame.tests;

import ChessGame.pieces.Piece;
import ChessGame.Chessboard;
import ChessGame.pieces.Queen;
import junit.framework.TestCase;

import static ChessGame.pieces.Piece.BLACK;
import static ChessGame.pieces.Piece.WHITE;

public class QueenTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    public void testConstructor() {
        chessboard.setTheBoard();
        Piece piece = new Queen(BLACK);
        assertEquals(BLACK, piece.getColor());
        assertEquals(Piece.QUEEN, piece.getType());
        assertEquals('♛', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }

    public void testMoveAlongRank(){
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(0 , 3, chessboard));
    }

    public void testMoveAlongFile() {
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(3 , 5, chessboard));
    }

    public void testMoveDiagonally() {
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(4 , 4, chessboard));
    }

    public void testLeapOver() {
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        Piece piece1 = new Queen(WHITE);
        chessboard.setBoard(3, 4, piece1);
        assertEquals(false, piece.canMove(3 , 5, chessboard));
    }

    public void testMoveTopLeft() {
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(2 , 2, chessboard));
    }

    public void testMoveTopRight() {
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(4 , 2, chessboard));
    }

    public void testMoveBottomLeft() {
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(2 , 4, chessboard));
    }

    public void testMoveBottomRight() {
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(5 , 5, chessboard));
    }

    public void testMoveNotDiagonally(){
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(3 , 5, chessboard));
    }

    public void testLeapOver2(){
        Piece piece = new Queen(WHITE);
        chessboard.setBoard(3, 3, piece);
        Piece piece1 = new Queen(WHITE);
        chessboard.setBoard(2, 4, piece1);
        assertEquals(false, piece.canMove(1 , 5, chessboard));
    }

}