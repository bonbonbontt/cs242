package ChessGame.tests;

import ChessGame.Chessboard;
import ChessGame.pieces.King;
import ChessGame.pieces.Piece;
import junit.framework.TestCase;
import ChessGame.pieces.Amazon;

public class AmazonTest extends TestCase {
    Chessboard chessboard = new Chessboard(8, 8);


    public void testCanMove() throws Exception {
        Piece amazon = new Amazon(Piece.WHITE);
        chessboard.setBoard(7, 7, amazon);
        Piece king = new King(Piece.WHITE);
        chessboard.setBoard(5, 5, king);
        assertEquals(true, amazon.canMove(6, 6, chessboard));
    }

}