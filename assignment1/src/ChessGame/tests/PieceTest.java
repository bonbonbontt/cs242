package ChessGame.tests;

import ChessGame.*;
import junit.framework.TestCase;
import ChessGame.pieces.Bishop;
import ChessGame.pieces.King;
import ChessGame.pieces.Piece;

import static ChessGame.pieces.Piece.BLACK;
import static ChessGame.pieces.Piece.WHITE;

public class PieceTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    public void testGetColor() throws Exception {
        Piece piece = new King(Piece.WHITE);
        assertEquals(Piece.WHITE, piece.getColor());
    }

    public void testGetType() throws Exception {
        Piece piece = new King(Piece.WHITE);
        assertEquals(Piece.KING, piece.getType());
    }

    public void testGetIsAlive() throws Exception {
        Piece piece = new King(Piece.WHITE);
        assertEquals(true, piece.getIsAlive());
    }

    public void testGetSymbol() throws Exception {
        Piece piece = new King(Piece.WHITE);
        assertEquals('♔', piece.getSymbol());
    }

    public void testSetColor() throws Exception {
        Piece piece = new King(Piece.WHITE);
        piece.setColor(Piece.BLACK);
        assertEquals(Piece.BLACK, piece.getColor());

    }

    public void testSetType() throws Exception {
        Piece piece = new King(Piece.WHITE);
        piece.setType(Piece.QUEEN);
        assertEquals(Piece.QUEEN, piece.getType());

    }

    public void testSetIsAlive() throws Exception {
        Piece piece = new King(Piece.WHITE);
        piece.setIsAlive(false);
        assertEquals(false, piece.getIsAlive());
    }

    public void testSetSymbol() throws Exception {
        Piece piece = new King(Piece.WHITE);
        piece.setSymbol('a');
        assertEquals('a', piece.getSymbol());
    }

    public void testIsValidPiece() throws Exception {
        assertEquals(false, Piece.isValidPiece(null, 1));
        Piece piece = new King(Piece.WHITE);
        assertEquals(true, Piece.isValidPiece(piece, 0));
        piece.setIsAlive(false);
        assertEquals(false, Piece.isValidPiece(piece, Player.WHITE));
        piece.setIsAlive(true);
        assertEquals(false, Piece.isValidPiece(piece, Player.BLACK));

    }

    public void testCanMove() throws Exception {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(5, 5, chessboard));
    }

    public void testMoveAndCapture() throws Exception {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(0, 0, piece);
        Piece piece2 = new Bishop(BLACK);
        chessboard.setBoard(1, 0, piece2);
        int[] xy = {0,0};
        chessboard.getMap().put(piece, xy);
        int[] xy2 = {1, 0};
        chessboard.getMap().put(piece2, xy2);
        piece.moveAndCapture(1, 0, chessboard);
        assertEquals(1, chessboard.getCoordinatesByPiece(piece)[0]);
        assertEquals(0, chessboard.getCoordinatesByPiece(piece)[1]);
    }

    public void testMove() throws Exception {
        Piece piece = new Bishop(WHITE);
        chessboard.setBoard(0, 0, piece);
        piece.move(1, 1, chessboard);
        assertEquals(piece, chessboard.getPieceByPosition(1, 1));
        int[] xy = {1, 1};
        assertEquals(xy[0], chessboard.getCoordinatesByPiece(piece)[0]);
        assertEquals(xy[1], chessboard.getCoordinatesByPiece(piece)[1]);
    }

    public void testCapture() throws Exception {
        Piece piece = new Bishop(WHITE);
        Piece piece2 = new Bishop(BLACK);
        chessboard.setBoard(0, 0, piece);
        chessboard.setBoard(1, 0, piece2);
        piece.capture(1, 0, chessboard);
        assertEquals(null, chessboard.getPieceByPosition(1, 0));
        assertEquals(piece, chessboard.getPieceByPosition(0, 0));
    }

    public void testIsOpponent() throws Exception {
        Piece piece = new Bishop(WHITE);
        Piece piece2 = new Bishop(BLACK);
        Piece piece3 = new Bishop(BLACK);
        assertEquals(true, piece.isOpponent(piece2));
        assertEquals(false, piece3.isOpponent(piece2));
    }

    public void testIsKing() throws Exception {
        Piece piece = new King(WHITE);
        assertEquals(true, piece.isKing());
        Piece piece2 = new Bishop(BLACK);
        assertEquals(false, piece2.isKing());

    }

}