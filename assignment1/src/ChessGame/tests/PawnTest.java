package ChessGame.tests;

import ChessGame.pieces.Piece;
import ChessGame.Chessboard;
import ChessGame.pieces.Pawn;
import junit.framework.TestCase;

import static ChessGame.pieces.Piece.BLACK;
import static ChessGame.pieces.Piece.WHITE;

public class PawnTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);
    //Test constructor
    public void testConstructor() {
        chessboard.setTheBoard();
        Piece piece = new Pawn(BLACK);
        assertEquals(BLACK, piece.getColor());
        assertEquals(Piece.PAWN, piece.getType());
        assertEquals('♟', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }


    // Test moving forward to the unoccupied square immediately in front of it on the same file
    public void test1(){
        Piece piece = new Pawn(WHITE);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(3 , 2, chessboard));
    }

    // Test on its first move it may advance two squares along the same file provided both squares are unoccupied
    public void test2(){
        Piece piece = new Pawn(WHITE);
        chessboard.setBoard(0, 6, piece);
        assertEquals(true, piece.canMove(0 , 4, chessboard));
    }

    // Test it may move to a square occupied by an opponent's piece which is diagonally in front of it on an adjacent file,
    // capturing that piece.
    public void test3(){
        Piece piece = new Pawn(WHITE);
        Piece opponent = new Pawn(BLACK);
        chessboard.setBoard(1, 6, piece);
        chessboard.setBoard(0, 5, opponent);
        assertEquals(true, piece.canMove(0 , 5, chessboard));
    }

    //Test move two squares on second move
    public void testBadMove1(){
        Piece piece = new Pawn(WHITE);
        chessboard.setBoard(7, 6, piece);
        assertEquals(true, piece.canMove(7 , 4, chessboard));
        piece.move(7, 4, chessboard);
        assertEquals(false, piece.canMove(7 , 2, chessboard));

    }

    //Test move diagonally without opponent
    public void testBadMove2(){
        Piece piece = new Pawn(WHITE);
        chessboard.setBoard(7, 6, piece);
        assertEquals(false, piece.canMove(6 , 5, chessboard));
    }
}