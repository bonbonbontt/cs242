package ChessGame;

import ChessGame.pieces.*;

public class Utils {
    /**
     * Convert string input to coordinates stored in int array
     * @param input The string user enters
     * @return An int array where array[0] is x coordinate, array[y] is y coordinate
     */
    public static int[] convertInput2Coordinates(String input){
        int y = 8 - Character.getNumericValue(input.charAt(1));
        int x = input.charAt(0) - 97;
        int[] ret = new int[2];
        ret[0] = x;
        ret[1] = y;
        return ret;
    }

    /**
     * Return true if current position and target position is diagonal
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtX The x coordinate of the target position
     * @param tgtY The y coordinate of the target position
     * @return True if current position and target position is diagonal
     */
    public static boolean isDiagonally(int x, int y, int tgtX, int tgtY){
        return (Math.abs(x - tgtX)  == Math.abs(y - tgtY));
    }


    /**
     * Return true if the target position is in the topLeft of current position
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtX The x coordinate of the target position
     * @param tgtY The y coordinate of the target position
     * @return True if the target position is in the topLeft of current position
     */
    public static boolean isTopLeft(int x, int y, int tgtX, int tgtY){
        return ((tgtX - x) < 0 && (tgtY - y) < 0);
    }

    /**
     * Return true if the target position is in the top right of current position
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtX The x coordinate of the target position
     * @param tgtY The y coordinate of the target position
     * @return Return true if the target position is in the top right of current position
     */
    public static boolean isTopRight(int x, int y, int tgtX, int tgtY){
        return ((tgtX - x) > 0 && (tgtY - y) < 0);
    }

    /**
     * Return true if the target position is in the bottom left of current position
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtX The x coordinate of the target position
     * @param tgtY The y coordinate of the target position
     * @return Return true if the target position is in the bottom left of current position
     */
    public static boolean isBottomLeft(int x, int y, int tgtX, int tgtY){
        return ((tgtX - x) < 0 && (tgtY - y) > 0);
    }

    /**
     * Return true if the target position is in the bottom right of current position
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtX The x coordinate of the target position
     * @param tgtY The y coordinate of the target position
     * @return Return true if the target position is in the bottom right of current position
     */
    public static boolean isBottomRight(int x, int y, int tgtX, int tgtY){
        return ((tgtX - x) > 0 && (tgtY - y) > 0);
    }

    /**
     * Return true if the target position is in one square to the current position
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtx The x coordinate of the target position
     * @param tgty The y coordinate of the target position
     * @return Return true if the target position is in one square to the current position
     */
    public static boolean isInOneSquare(int x, int y, int tgtx, int tgty){
        //same file
        if (x == tgtx){return (Math.abs(y - tgty) <= 1);}
        //same rank
        if (tgty == y){return (Math.abs(x - tgtx) <= 1);}
        //diagonals
        return ((Math.abs(y - tgty) == 1) && (Math.abs(x - tgtx) == 1));
    }

    /**
     * Return true if the target position are current position is L shape
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtx The x coordinate of the target position
     * @param tgty The y coordinate of the target position
     * @return True if the target position are current position is L shape
     */
    public static boolean isLShape(int x, int y, int tgtx, int tgty){
        if(Math.abs(y - tgty) == 2){return (Math.abs(x - tgtx) == 1);}
        else if(Math.abs(y - tgty) == 1){return (Math.abs(x - tgtx) == 2);}
        return false;
    }

    /**
     * Return true if the target position is immediately in front of the current position
     * @param x The x coordinate of the piecex coordinate of the piece
     * @param y The y coordinate of the piecey coordinate of the piece
     * @param tgtx The x coordinate of the target position
     * @param tgty The y coordinate of the target position
     * @param piece The piece to be moved
     * @return Return true if the target position is immediately in front of the current position
     */
    public static boolean isInFrontOfImmediately(int x, int y, int tgtx, int tgty, Piece piece){
        if (piece.getColor() == Piece.BLACK){return ((tgty - y) == 1) && tgtx  == x;}
        else {return ((y - tgty) == 1) && tgtx == x;}
    }


    /**
     * Return true if the target is in front of the current position and is on the diagonal
     * @param x The x coordinate of the piecex coordinate of the piece
     * @param y The y coordinate of the piecey coordinate of the piece
     * @param tgtx The x coordinate of the target position
     * @param tgty The y coordinate of the target position
     * @param piece The piece to be moved
     * @return Return true if the target is in front of the current position and is on the diagonal
     */
    public static boolean isDiagonallyInFront(int x, int y, int tgtx, int tgty, Piece piece){
        if(piece.getColor() == Piece.BLACK){ return Utils.isDiagonally(x, y, tgtx, tgty) && (y - tgty) < 0; }
        else{ return Utils.isDiagonally(x, y, tgtx, tgty) && (tgty - y) < 0; }
    }


    /**
     * Return true if there's any chess piece between current position and target position, only diagonally
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @param tgtX The x coordinate of the target position
     * @param tgtY The y coordinate of the target position
     * @param chessboard
     * @return True if there's any chess piece between current position and target position, only diagonally
     */
    public static boolean hasChessOnTheWayDiagonally(int x, int y, int tgtX, int tgtY, Chessboard chessboard) {
        int h = Math.abs(x - tgtX);
        if (isTopLeft(x, y, tgtX, tgtY)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getPieceByPosition((x - i), (y - i)) != null) {return true; }
            }
        } else if (isTopRight(x, y, tgtX, tgtY)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getPieceByPosition((x + i), (y - i)) != null) { return true; }
            }
        } else if (isBottomLeft(x, y, tgtX, tgtY)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getPieceByPosition((x - i), (y + i)) != null) { return true; }
            }
        } else if (isBottomRight(x, y, tgtX, tgtY)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getPieceByPosition((x + i), (y + i)) != null) { return true; }
            }
        }
        return false;
    }

    /**
     * Return true if the two immediately front square is unoccupied
     * @param x The x coordinate of the piecex coordinate of the piece
     * @param y The y coordinate of the piecey coordinate of the piece
     * @param tgtx The x coordinate of the target position
     * @param tgty The y coordinate of the target position
     * @param chessboard The chessboard all ChessGame.pieces move
     * @param piece The piece to be moved
     * @return Return true if the two immediately front square is unoccupied
     */
    public static boolean isTwoUnoccupied(int x, int y, int tgtx, int tgty, Chessboard chessboard, Piece piece){
        if(piece == null)return false;
        if (piece.getColor() == Piece.BLACK){
            boolean is2FrontImmediately = (tgty - y) == 2;
            boolean oneNoChess = chessboard.isUnoccupied(x, (y+1));
            boolean twoNoChess = chessboard.isUnoccupied(x, (y+2));
            return is2FrontImmediately && oneNoChess && twoNoChess;
        }
        else{
            boolean is2FrontImmediately = (y - tgty) == 2;
            boolean oneNoChess = chessboard.isUnoccupied(x, y-1);
            boolean twoNoChess = chessboard.isUnoccupied(x, y-2);
            return is2FrontImmediately && oneNoChess && twoNoChess;
        }
    }

    /**
     * Return true if there's any chess piece between current position and target position, only along any file or rank
     * @param x The x coordinate of the piecex coordinate of the piece
     * @param y The y coordinate of the piecey coordinate of the piece
     * @param tgtx The x coordinate of the target position
     * @param tgty The y coordinate of the target position
     * @param chessboard The board where all ChessGame.pieces move
     * @return True if there's any chess piece between current position and target position, only along any file or rank
     */
    public static boolean hasChessOnTheWay(int x, int y, int tgtx, int tgty, Chessboard chessboard){
        if(isSameRank(y, tgty)){
            if(tgtx > x){
                for (int i = x + 1; i < tgtx; i ++){
                    if (chessboard.getPieceByPosition(i, y) != null){return true;}
                }
            }
            else {
                for (int i = x - 1; i > tgtx; i --){
                    if (chessboard.getPieceByPosition(i, y) != null)return true;
                }
            }
        }
        else if (isSameFile(x, tgtx)) {
            if(tgty > y){
                for (int i = y + 1; i < tgty; i ++){
                    if (chessboard.getPieceByPosition(x, i) != null){return true;}
                }
            }
            else {
                for (int i = y - 1; i > tgty; i --){
                    if (chessboard.getPieceByPosition(x, i) != null){return true;}
                }
            }
        }
        return false;
    }

    /**
     * Generate all ChessGame.pieces for a single player
     * @param color The color of the ChessGame.pieces
     * @return Piece array for a single player
     */
    public static Piece[] generateNewPieces(int color, boolean customized){
        Piece[] pieces = new Piece[Player.NUMBEROFPIECES];
        pieces[0] = new King(color);
        pieces[1] = new Queen(color);
        int index = 2;
        for (int i = 0; i < 2; i ++){
            pieces[index] = new Bishop(color);
            pieces[index + 2] = new Knight(color);
            pieces[index + 4] = new Rook(color);
            index ++;
        }
        if(customized){
            for (int i = 8; i < 14; i++){
                pieces[i] = new Pawn(color);
            }
            pieces[14] = new Hopper(color);
            pieces[15] = new Amazon(color);
        }
        else{
            for (int i = 8; i < 16; i++){
                pieces[i] = new Pawn(color);
            }
        }

        return pieces;
    }

    /**
     * Return true if the target position is in the adjacent file of the current position
     * @param x The x coordinate of the current position
     * @param tgtx The x coordinate of the target position
     * @return Return true if the target position is in the adjacent file of the current position
     */
    public static boolean isAdjacentFile(int x, int tgtx){
        return Math.abs(x - tgtx) == 1;
    }

    /**
     * Return true if the target position is in the same rank as the chess
     * @param y The y coordinate of the current position
     * @param tgty The y coordinate of the target position
     * @return Return true if the target position is in the same rank as the chess
     */
    public static boolean isSameRank(int y, int tgty){
        return (tgty == y);
    }

    /**
     * Return true the target position is in the same file as the chess
     * @param x The x coordinate of the piece
     * @param tgtX
     * @return True the target position is in the same file as the chess
     */
    public  static boolean isSameFile(int x, int tgtX){
        return (tgtX == x);
    }

    public static boolean canJump(int x, int y, int tgtx, int tgty, Chessboard chessboard){
        if(!Utils.isSameFile(x, tgtx) && !Utils.isSameRank(y, tgty)){
            return false;
        }
        if(Utils.isInOneSquare(x, y, tgtx, tgty)){return false;}
        if (Utils.isSameFile(x, tgtx)){
            if(tgty > y){
                if(chessboard.isUnoccupied(x, tgty) && chessboard.isUnoccupied(x, tgty-1)) return false;
                for(int i = y + 1; i < tgty; i+=2){ if(chessboard.isUnoccupied(x, i) || i >= tgty)return false; }
                for(int i = y + 2; i < tgty; i+=2){ if(!chessboard.isUnoccupied(x, i) || i >= tgty)return false; }
            }
            else{
                if(chessboard.isUnoccupied(x, tgty) && chessboard.isUnoccupied(x, tgty+1)) return false;
                for(int i = y - 1; i > tgty; i-=2){ if(chessboard.isUnoccupied(x, i) || i <= tgty)return false; }
                for(int i = y - 2; i > tgty; i-=2){ if(!chessboard.isUnoccupied(x, i) || i <= tgty)return false; }
            }
        }
        else if (Utils.isSameRank(y, tgty)){
            if(tgtx > x){
                if(chessboard.isUnoccupied(tgtx, y) && chessboard.isUnoccupied(tgtx-1, y)) return false;
                for(int i = x + 1; i < tgtx; i+=2){ if(chessboard.isUnoccupied(i, y) || i >= tgtx){return false;} }
                for(int i = x + 2; i < tgtx; i+=2){ if(!chessboard.isUnoccupied(i, y) || i >= tgtx)return false; }
            }
            else{
                if(chessboard.isUnoccupied(tgtx, y) && chessboard.isUnoccupied(tgtx+1, y)) return false;
                for(int i = x - 1; i > tgty; i-=2){ if(chessboard.isUnoccupied(i, y) || i <= tgtx)return false; }
                for(int i = x - 2; i > tgty; i-=2){ if(!chessboard.isUnoccupied(i, y) || i <= tgtx)return false; }
            }
        }
        return true;
    }
}
