package ChessGame;

import ChessGame.pieces.Piece;

public class Player {
    // Colors
    public static int WHITE = 0;
    public static int BLACK = 1;
    public static int NUMBEROFPIECES = 16;
    private int numberOfAlivePieces = NUMBEROFPIECES; //number of alive ChessGame.pieces
    private int color;
    private String name;
    private int score = 0;

    public static int PAWN = 1;
    public static int KNIGHT = 3;
    public static int BISHOP = 3;
    public static int ROOK = 5;
    public static int HOPPER = 6;
    public static int AMAZON = 7;
    public static int QUEEN = 9;
    public static int KING = 10;
    public static int WIN = 50;


    //getters and setters
    public int getColor(){return this.color;}
    public int getNumberOfAlivePieces(){return this.numberOfAlivePieces;}
    public void setNumberOfAlivePieces(int num){this.numberOfAlivePieces = num;}

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getScore(){return this.score;}

    public void setScore(int score) {
        this.score = score;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public void minusScore(int score){
        this.score -= score;
    }
    public void addScoreByPiece(Piece piece){
        if(piece.getType() == Piece.PAWN) addScore(PAWN);
        if(piece.getType() == Piece.KNIGHT) addScore(KNIGHT);
        if(piece.getType() == Piece.BISHOP) addScore(BISHOP);
        if(piece.getType() == Piece.ROOK) addScore(ROOK);
        if(piece.getType() == Piece.QUEEN) addScore(QUEEN);
        if(piece.getType() == Piece.KING) addScore(KING);
        if(piece.getType() == Piece.HOPPER) addScore(HOPPER);
        if(piece.getType() == Piece.AMAZON) addScore(AMAZON);
    }

    public void minusScoreByPiece(Piece piece){
        if(piece.getType() == Piece.PAWN) minusScore(PAWN);
        if(piece.getType() == Piece.KNIGHT) minusScore(KNIGHT);
        if(piece.getType() == Piece.BISHOP) minusScore(BISHOP);
        if(piece.getType() == Piece.ROOK)   minusScore(ROOK);
        if(piece.getType() == Piece.QUEEN)  minusScore(QUEEN);
        if(piece.getType() == Piece.KING)   minusScore(KING);
        if(piece.getType() == Piece.HOPPER) minusScore(HOPPER);
        if(piece.getType() == Piece.AMAZON) minusScore(AMAZON);
    }

    //constructor to set up white or black
    public Player(int color){
        this.color = color;
    }

    /**
     * Decrease the number of alive ChessGame.pieces by one
     */
    public void decreaseNumberOfPieces(){
        this.numberOfAlivePieces --;
    }
}
