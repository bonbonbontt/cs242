package ChessGame;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Format {
    /**
     * Print the string used to let user enter the position of the piece he wants to move
     */
    public static void enterPiecePosition() {
        System.out.print("Please enter the position of the piece you want to move:\n");
    }

    /**
     * This function gets called whenever a user need to enter the target position for a piece to move
     */
    public static void enterTgtPosition() {
        System.out.print("Please enter the target position:\n");
    }

    /**
     * This function get called whenever a piece is out of bound on chessboard
     */
    public static void outOfBound(){
        System.out.println("Error: out of bound!");
    }

    /**
     * This function gets called whenever a piece is moving to an invalid position because of rules
     */
    public static void invalidPosition(){
        System.out.println("Invalid Position!");
    }

    /**
     * The function gets called whenever a piece is dead and a user wants to move it
     */
    public static void doesNotExist(){System.out.print("This chess does not exist!\n");}

    /**
     * This function gets called whenever a user wants to move opponent's piece
     */
    public static void notYourPiece(){System.out.print("This is not your piece!\n");}

    /**
     * Print the game banner
     * @param currentPlayerColor Current play's color
     */
    public static void printBanner(int currentPlayerColor){
        String currentPlayer;
        if (currentPlayerColor == Player.WHITE){currentPlayer = "Player1";}
        else{currentPlayer = "Player2";}
        System.out.println("\n==================================");
        System.out.println("Player1: White      Player2: Black");
        System.out.println(currentPlayer + "'s move");
        System.out.println("==================================");
    }

    /**
     * print winner
     */
    public static void printWinner(int currentPlayerColor){
        String currentPlayer;
        if (currentPlayerColor == Player.WHITE){
            currentPlayer = "Player1";
            System.out.println(currentPlayer + " won! Congratulations!\n");
        }
        else if (currentPlayerColor == Player.BLACK) {
            currentPlayer = "Player2";
            System.out.println(currentPlayer + " won! Congratulations!\n");
        }
        else if (currentPlayerColor == -1){
            System.out.println("Draw!\n");
        }
    }



}