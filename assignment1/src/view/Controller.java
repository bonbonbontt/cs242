package view;

import ChessGame.Chessboard;
import ChessGame.Player;
import ChessGame.pieces.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Controller {
    private Chessboard chessboard;
    private View view;
    private int[] currentPosition = {-1, -1};
    private boolean customized = false;
    private ArrayList scores1 = new ArrayList();
    private ArrayList scores2 = new ArrayList();
    private Stack<HashMap<Piece, int[][]>> movements = new Stack<HashMap<Piece, int[][]>>();
    private boolean endGame = false;



    public ArrayList getScores1() {
        return scores1;
    }

    public ArrayList getScores2() {
        return scores2;
    }

    public int[] getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int x, int y) {
        this.currentPosition[0] = x;
        this.currentPosition[1] = y;

    }

    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
    }

    /**
     * Constructor
     * @param chessboard
     * @param view
     */
    public Controller(Chessboard chessboard, View view){
        this.chessboard = chessboard;
        this.view = view;
        this.addRestartActionListener();
        this.addStartActionListener();
        this.addForfeitActionListner();
        this.addScoreActionListener();
        this.addUndoActionListener();

    }

    /**
     * add Score aciton listner on info panel
     */
    public void addScoreActionListener(){
        JMenuItem score = this.view.getScore();
        score.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //check not started yet
                if(chessboard.getPlayer1().getName() == null || chessboard.getPlayer1().getName().length() == 0){
                    showGameNotStartedYet();
                    return;
                }
                showScoreMessage();
            }
        });

    }

    /**
     *
     * add undo action listener
     */
    public void addUndoActionListener(){
        JMenuItem undo = this.view.getUndo();
        Controller controller = this;
        undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //check not started yet
                if(chessboard.getPlayer1().getName() == null || chessboard.getPlayer1().getName().length() == 0){
                    showGameNotStartedYet();
                    return;
                }

                if(controller.movements.size() == 0) {return;}
                chessboard.undo((HashMap<Piece, int[][]>)controller.movements.pop());
                controller.undo();
                chessboard.showChessboard(-1, -1);
                updateInfoScore();
                chessboard.flipCurrentPlayer();
                flipInfoPanelColor();
                View.resetBackgroundColor(view.getButtons());
                view.getChessboardPanel().repaint();
                view.getInfoPanel().repaint();
            }
        });
    }

    /**
     * Get icon by piece
     * @param piece
     * @return
     */
    public ImageIcon getIconByPiece(Piece piece){
        if(piece == null)return null;
        if(piece.getColor() == Piece.BLACK){
            if(piece.getType() == Piece.ROOK) return new ImageIcon(this.view.getImages()[8]);
            if(piece.getType() == Piece.KNIGHT) return new ImageIcon(this.view.getImages()[6]);
            if(piece.getType() == Piece.BISHOP) return new ImageIcon(this.view.getImages()[4]);
            if(piece.getType() == Piece.KING) return new ImageIcon(this.view.getImages()[2]);
            if(piece.getType() == Piece.QUEEN) return new ImageIcon(this.view.getImages()[0]);
            if(piece.getType() == Piece.PAWN) return new ImageIcon(this.view.getImages()[10]);
            if(piece.getType() == Piece.AMAZON) return new ImageIcon(this.view.getImages()[12]);
            if(piece.getType() == Piece.HOPPER) return new ImageIcon(this.view.getImages()[14]);
        }
        else{
            if(piece.getType() == Piece.ROOK) return new ImageIcon(this.view.getImages()[9]);
            if(piece.getType() == Piece.KNIGHT) return new ImageIcon(this.view.getImages()[7]);
            if(piece.getType() == Piece.BISHOP) return new ImageIcon(this.view.getImages()[5]);
            if(piece.getType() == Piece.KING) return new ImageIcon(this.view.getImages()[3]);
            if(piece.getType() == Piece.QUEEN) return new ImageIcon(this.view.getImages()[1]);
            if(piece.getType() == Piece.PAWN) return new ImageIcon(this.view.getImages()[11]);
            if(piece.getType() == Piece.AMAZON) return new ImageIcon(this.view.getImages()[13]);
            if(piece.getType() == Piece.HOPPER) return new ImageIcon(this.view.getImages()[15]);
        }
        return null;
    }

    /**
     * undo one step
     */
    public void undo(){
        for(int i = 0; i < this.chessboard.getLength(); i++){
            for (int j = 0; j < this.chessboard.getWidth(); j++){
                Piece piece = this.chessboard.getPieceByPosition(j, i);
                this.view.getButtonByPosition(j, i).setIcon(this.getIconByPiece(piece));
            }
        }
    }

    /**
     * add forfeit action listener
     */
    public void addForfeitActionListner(){

        JMenuItem forfeit = this.view.getForfeit();
        Chessboard chessboard = this.chessboard;
        Controller controller = this;

        forfeit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(endGame)return;
                //check not started yet
                if(chessboard.getPlayer1().getName() == null || chessboard.getPlayer1().getName().length() == 0){
                    showGameNotStartedYet();
                    return;
                }
                String name = chessboard.getPlayerByColor(chessboard.getCurrentPlayerColor()).getName();
                if(confirmForfeit() == JOptionPane.YES_OPTION){
                    chessboard.getPlayerByColor(chessboard.getOpponentsColor(chessboard.getCurrentPlayerColor())).setScore(Player.WIN);
                    controller.getScores1().add(chessboard.getPlayer1().getScore());
                    controller.getScores2().add(chessboard.getPlayer2().getScore());
                    initializeRestart();
                }
            }
        });
    }


    public void addListeners(){
        this.addMouseListner2Buttons();
    }

    /**
     * add mouse listener
     */
    public void addMouseListner2Buttons(){
        JButton[][] buttons = this.view.getButtons();
        for(int i = 0; i < buttons.length; i++){
            for(int j = 0; j < buttons[0].length; j++){
                buttons[i][j].addMouseListener(Controller.creatMouseListener(this.view, buttons, this.chessboard, this));
            }
        }
    }

    /**
     * move the piece and push to stack
     * @param piece
     * @param tgtxy
     */
    public void movePiece(Piece piece, int[] tgtxy){
        //push the future move to stack
        int[][] xy = {{currentPosition[0], currentPosition[1]}, tgtxy};
        HashMap move = new HashMap<Piece, int[][]>();
        move.put(piece, xy);
        System.out.println("\n"+"move:" + currentPosition[0] + "," + currentPosition[1] + " to " + tgtxy[0] + ","+tgtxy[1]);

        if(chessboard.getPieceByPosition(tgtxy[0], tgtxy[1]) != null){
            int[][] xxyy = {tgtxy, null};
            move.put(chessboard.getPieceByPosition(tgtxy[0], tgtxy[1]), xxyy);
        }
        this.movements.push(move);

        //move
        piece.moveAndCapture(tgtxy[0], tgtxy[1], chessboard);
        Icon icon = this.view.getButtons()[currentPosition[1]][currentPosition[0]].getIcon();
        this.view.getButtons()[currentPosition[1]][currentPosition[0]].setIcon(null);
        this.view.getButtons()[tgtxy[1]][tgtxy[0]].setIcon(icon);
    }

    /**
     * flip info lanel color
     */
    public void flipInfoPanelColor(){
        if(this.view.getInfoPanel().getBackground() == View.DARKBROWN){ this.view.getInfoPanel().setBackground(View.LIGHTBROWN);}
        else{this.view.getInfoPanel().setBackground(View.DARKBROWN);}
        this.view.getInfoPanel().repaint();
    }

    /**
     * update info score
     */
    public void updateInfoScore(){
        this.view.getLabels()[0][3].setText(Integer.toString(this.chessboard.getPlayer1().getScore()));
        this.view.getLabels()[1][3].setText(Integer.toString(this.chessboard.getPlayer2().getScore()));
        System.out.print(Integer.toString(this.chessboard.getPlayer1().getScore()) + ":" + (Integer.toString(this.chessboard.getPlayer2().getScore())));
        this.view.getInfoPanel().repaint();

    }


    /**
     * create mouse listener
     * @param view
     * @param buttons buttons in the chessboard panel
     * @param chessboard
     * @param controller
     * @return
     */
    public static MouseListener creatMouseListener(View view, JButton[][] buttons, Chessboard chessboard, Controller controller){
        MouseListener mouseListener = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) { }

            @Override
            public void mousePressed(MouseEvent e) {
//                if(controller.endGame)return;
                JButton button = (JButton) e.getSource();

                //if background is green, then move the piece
                if(button.getBackground() == View.GREEN){
                    int[] xy = controller.getCurrentPosition();
                    int[] tgtxy = getPositionByMouseEvent(e);
                    Piece piece = chessboard.getPieceByPosition(xy[0], xy[1]);

                    //check if the piece is in the same color as the current player
                    if(piece.getColor() != chessboard.getCurrentPlayerColor()){
                        System.out.println(piece.getSymbol());
                        System.out.println(piece.getColor() + "," + chessboard.getCurrentPlayerColor());
                        controller.showNotYourChess(controller.view.getWindow());
                        return;
                    }
                    controller.movePiece(piece, tgtxy);
                    View.resetBackgroundColor(buttons);
                    button.setBackground(Color.orange);
                    controller.setCurrentPosition(-1, -1);


                    //add score
                    controller.updateInfoScore();

                    //check king dead
                    if(chessboard.checkEnd(chessboard.getCurrentPlayerColor())) {
                        controller.showWin(chessboard.getPlayerByColor(chessboard.getCurrentPlayerColor()).getName());
                    }

                    //change the turn
                    chessboard.flipCurrentPlayer();
                    controller.flipInfoPanelColor();

                    //check checkMate
                    if(chessboard.isCheckMate(chessboard.getCurrentPlayerColor())){
                        controller.showCheckMate(chessboard.getPlayerByColor(chessboard.getOpponentsColor(chessboard.getCurrentPlayerColor())).getName());
                    }

                    //check stalemate
                    if(chessboard.isStaleMate(chessboard.getCurrentPlayerColor())){
                        controller.showStaleMate(chessboard.getPlayerByColor(chessboard.getOpponentsColor(chessboard.getCurrentPlayerColor())).getName());

                    }

                    return;
                }
                View.resetBackgroundColor(buttons);

                //mark current position blue, set it as current position
                button.setBackground(View.BLUE);
                int[] xy = getPositionByMouseEvent(e);
                controller.setCurrentPosition(xy[0], xy[1]);

                //mark potential position green
                Piece piece = chessboard.getPieceByPosition(xy[0], xy[1]);
                int[][] potentialPositions = chessboard.getPotentialPositions(piece);
                if(potentialPositions == null)return;
                for(int i = 0; i < potentialPositions.length; i++){
                    view.getButtonByPosition(potentialPositions[i][0], potentialPositions[i][1]).setBackground(View.GREEN);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
//                if(controller.endGame)return;

                JButton button = (JButton) e.getSource();
                if(controller.getCurrentPosition()[0] != -1)return;
                if(button.getBackground() == View.BLUE || button.getBackground() == View.GREEN)return;
                View.resetBackgroundColor(buttons);

                //mark current position light blue
                button.setBackground(View.LIGHTBLUE);

                //mark potential position light green
                int[] xy = getPositionByMouseEvent(e);
                Piece piece = chessboard.getPieceByPosition(xy[0], xy[1]);
                int[][] potentialPositions = chessboard.getPotentialPositions(piece);
                if(potentialPositions == null)return;
                for(int i = 0; i < potentialPositions.length; i++){
                    view.getButtonByPosition(potentialPositions[i][0], potentialPositions[i][1]).setBackground(View.LIGHTGREEN);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
//                if(controller.endGame)return;

                JButton button = (JButton) e.getSource();
                if(button.getBackground() == View.BLUE || button.getBackground() == View.GREEN)return;
                View.setAllBackgroundToOriginalColor(buttons);
                int[] xy =getPositionByMouseEvent(e);
                if ((xy[0] % 2 == 1 && xy[1] % 2 == 1)  || (xy[0] % 2 == 0 && xy[1] % 2 == 0)) {
                    button.setBackground(View.LIGHTBROWN);
                } else {
                    button.setBackground(View.DARKBROWN);
                }
            }
        };
        return mouseListener;
    }



    public static int[] getPositionByMouseEvent(MouseEvent e){
        JButton button = (JButton) e.getSource();
        int x = (button.getX()-10)/60;
        int y = (button.getY()-10)/61;
        int[] xy = {x, y};
        return xy;
    }

    public boolean confirmRestart(){
        int reply1 = JOptionPane.showConfirmDialog(
                this.view.getWindow(),
                String.format("%s, do you want to restart the game?",this.chessboard.getPlayer1().getName()),
                "Restart the game?",
                JOptionPane.YES_NO_OPTION
        );
        if(reply1 != JOptionPane.YES_OPTION){return false;}
        int reply2 = JOptionPane.showConfirmDialog(
                this.view.getWindow(),
                String.format("%s, do you want to restart the game?",this.chessboard.getPlayer2().getName()),
                "Restart the game?",
                JOptionPane.YES_NO_OPTION
        );
        if(reply2 != JOptionPane.YES_OPTION){return false;}
        return true;
    }

    public void initializeRestart(){
        View view = this.view;
        view.initializeIcon();
        chessboard.setTheBoard();
        chessboard.resetAllPieces();
        chessboard.setCurrentPlayerColor(Player.WHITE);
        chessboard.showChessboard(-1, -1);
        this.updateInfoScore();
        this.view.getInfoPanel().repaint();

    }


    public void addRestartActionListener(){

        JMenuItem restart = this.view.getRestart();
        Controller controller = this;

        restart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(chessboard.getPlayer1().getName() == null || chessboard.getPlayer1().getName().length() == 0){
                    showGameNotStartedYet();
                    return;
                }
                if(!controller.confirmRestart())return;
                controller.getScores1().add(-1);
                controller.getScores2().add(-1);
                controller.initializeRestart();
            }
        });
    }

    private int showChooseMode(){
        int reply = JOptionPane.showConfirmDialog(
                this.view.getWindow(),
                String.format("Do you want to play with customized pieces?",this.chessboard.getPlayer2().getName()),
                "Choose mode",
                JOptionPane.YES_NO_OPTION
        );
        return reply;
    }

    private void setMode(int mode){
        if(mode == JOptionPane.YES_OPTION){
            this.setCustomized(true);
            this.chessboard.setCustomized(true);
            this.view.setCustomized(true);
        }
        else{
            this.setCustomized(false);
            this.chessboard.setCustomized(false);
            this.view.setCustomized(false);
        }
    }

    public String[] addStartActionListener(){
        JMenuItem start = this.view.getStart();
        JFrame window = this.view.getWindow();
        String[] names = new String[2];
        View view = this.view;
        Chessboard chessboard = this.chessboard;
        Controller controller = this;

        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //choose mode
                setMode(showChooseMode());

                names[0] = JOptionPane.showInputDialog(window,"Player1's name:","White", JOptionPane.QUESTION_MESSAGE);
                if(names[0] == null)return;
                names[1] = JOptionPane.showInputDialog(window,"Player2's name:", "Black", JOptionPane.QUESTION_MESSAGE);
                if(names[1] == null)return;

                //set info panel
                if(names[0].length() == 0){names[0] = "player1";}
                if(names[1].length() == 0){names[1] = "player2";}
                view.setPlayerNameLabel(names[0], View.WHITE);
                view.setPlayerNameLabel(names[1], View.BLACK);
                chessboard.getPlayer1().setName(names[0]);
                chessboard.getPlayer2().setName(names[1]);
                initializeRestart();

            }
        });
        return names;
    }

    public void showNotYourChess(JFrame window){
        JOptionPane.showMessageDialog(window,"Not your turn!","WARNING", JOptionPane.ERROR_MESSAGE);
    }

    public int confirmForfeit(){
        int reply = JOptionPane.showConfirmDialog(
                this.view.getWindow(),
                String.format("%s, do you want to forfeit?",this.chessboard.getPlayerByColor(this.chessboard.getCurrentPlayerColor()).getName()),
                "Forfeit?",
                JOptionPane.YES_NO_OPTION
        );
        return reply;
    }

    public void showGameNotStartedYet(){
        JOptionPane.showMessageDialog(this.view.getWindow(),"Please start the game first!","WARNING", JOptionPane.ERROR_MESSAGE);
    }

    public void showWin(String name){
        JOptionPane.showMessageDialog(this.view.getWindow(),String.format("%s wins!", name),"CONGRATS", JOptionPane.PLAIN_MESSAGE);
    }

    public void showCheckMate(String name){
        JOptionPane.showMessageDialog(this.view.getWindow(),String.format("Checkmate! %s wins!", name),"CONGRATS", JOptionPane.PLAIN_MESSAGE);
    }

    public void showStaleMate(String name){
        JOptionPane.showMessageDialog(this.view.getWindow(),String.format("Stalemate!", name),"DRAWN", JOptionPane.PLAIN_MESSAGE);
    }

    public void showScoreMessage(){
        int win1 = 0;
        int win2 = 0;
        for(int i = 0; i < this.getScores1().size(); i++){
            if((int)this.getScores1().get(i) > (int)this.getScores2().get(i)){
                win1++;
            }
            else if((int)this.getScores1().get(i) < (int)this.getScores2().get(i)){
                win2++;
            }
            else{
                win1++;
                win2++;
            }
        }
        String scores = String.format(
                "%s : %s = %d : %d",
                this.chessboard.getPlayer1().getName(),
                this.chessboard.getPlayer2().getName(),
                win1,
                win2
        );
        scores +="\n";
        for(int i = 0; i < this.getScores1().size(); i++) {
            if((int)this.getScores1().get(i)!=-1){
                scores  = scores + (int)this.getScores1().get(i) + " : " + (int)this.getScores2().get(i) + "\n";
            }
            else{
                scores = scores + "Restarted\n";
            }
        }
        JOptionPane.showMessageDialog(this.view.getWindow(),scores,"SCORE", JOptionPane.PLAIN_MESSAGE);
    }
}
