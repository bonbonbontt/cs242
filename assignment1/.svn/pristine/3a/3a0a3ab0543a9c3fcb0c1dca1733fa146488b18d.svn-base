package tests;

import ChessGame.*;
import junit.framework.TestCase;

public class BishopTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    //Test constructor
    public void testConstructor(){
        Chess chess = new Bishop(true,1, 1, 2);
        assertEquals(true, chess.getIsAlive());
        assertEquals(1, chess.getX());
        assertEquals(1, chess.getY());
        assertEquals('♝', chess.getSymbol());
        assertEquals(2, chess.getPlayer());
    }

    public void testMoveTopLeft() {
        Chess chess = chessboard.getChessByPosition(2, 7);
        chessboard.setBoard(3, 3, chess);
        chess.setX(3);
        chess.setY(3);
        assertEquals(true, chess.move(2 , 2, chessboard));
        assertEquals(chess, chessboard.getChessByPosition(2, 2));
    }

    public void testMoveTopRight() {
        Chess chess = chessboard.getChessByPosition(2, 7);
        chessboard.setBoard(3, 3, chess);
        chess.setX(3);
        chess.setY(3);
        assertEquals(true, chess.move(4 , 2, chessboard));
        assertEquals(chess, chessboard.getChessByPosition(4, 2));
    }

    public void testMoveBottomLeft() {
        Chess chess = chessboard.getChessByPosition(2, 7);
        chessboard.setBoard(3, 3, chess);
        chess.setX(3);
        chess.setY(3);
        assertEquals(true, chess.move(2 , 4, chessboard));
        assertEquals(chess, chessboard.getChessByPosition(2, 4));
    }

    public void testMoveBottomRight() {
        Chess chess = chessboard.getChessByPosition(2, 7);
        chessboard.setBoard(3, 3, chess);
        chess.setX(3);
        chess.setY(3);
        assertEquals(true, chess.move(5, 5, chessboard));
        assertEquals(chess, chessboard.getChessByPosition(5, 5));
    }

    public void testMoveNotDiagonally(){
        Chess chess = chessboard.getChessByPosition(2, 7);
        chessboard.setBoard(3, 3, chess);
        chess.setX(3);
        chess.setY(3);
        assertEquals(false, chess.move(3, 5, chessboard));
        assertEquals(null, chessboard.getChessByPosition(3, 5));
    }

    public void testLeapOver(){
        Chess chess = chessboard.getChessByPosition(2, 7);
        chessboard.setBoard(3, 3, chess);
        chess.setX(3);
        chess.setY(3);

        Chess chess1 = chessboard.getChessByPosition(0, 1);
        chessboard.setBoard(2, 4, chess1);
        chess1.setX(2);
        chess1.setY(4);
        assertEquals(chess1, chessboard.getChessByPosition(2, 4));

        assertEquals(false, chess.move(1, 5, chessboard));
        assertEquals(null, chessboard.getChessByPosition(1, 5));
    }

}