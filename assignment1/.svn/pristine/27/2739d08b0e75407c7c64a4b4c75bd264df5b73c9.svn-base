package ChessGame;

public class Chess {
    // location
    private int x;
    private int y;
    private boolean isAlive;
    private char symbol = ' ';
    private int player;

    // getters and setters
    public int getX(){return this.x;}
    public int getY(){return this.y;}
    public boolean getIsAlive(){return this.isAlive;}
    public char getSymbol(){return this.symbol;}
    public int getPlayer(){return this.player;}
    public void setX (int x){this.x = x;}
    public void setY (int y){this.y = y;}
    public void setIsAlive (boolean isAlive){this.isAlive = isAlive;}
    public void setSymbol(char symbol){this.symbol = symbol;}
    public void setPlayer(int playerNumber){this.player = playerNumber;}

    //Return true if the current chess piece is currentPlayer's and is alive
    public boolean isValidChess(int currentPlayer){
        //check is_existent and is_alive
        if (!this.getIsAlive()) {
            System.out.print("This chess does not exist!\n");
            return false;
        }
        //check this chess is the current player's
        if (this.getPlayer() != currentPlayer){
            System.out.print("This is not your chess!\n");
            return false;
        }
        return true;
    }

    //Return true if it's valid to move to the target position
    public boolean canMove(int x, int y, Chessboard chessboard){return false;}

    //Return true if it's valid move and move the chess
    public boolean move(int x, int y, Chessboard chessboard){
        if (this.canMove(x, y, chessboard)){
            this.doMove(x, y, chessboard);
            return true;
        }
        else{
            return false;
        }
    }


    //Move the chess to the target position. Update chess, update chessboard.
    public void doMove(int x, int y, Chessboard chessboard){
        chessboard.setBoard(this.x, this.y, null);
        this.setX(x);
        this.setY(y);
        Chess tgtChess = chessboard.getChessByPosition(x, y);
        if (tgtChess != null && tgtChess.isOpponent(x, y, chessboard)){
            this.capture(x, y, chessboard);
        }
        chessboard.setBoard(x, y, this);
        return;
    }


    //Return true the target position is in the same file as the chess
    public boolean isSameFile(int x){
        return (this.getX() == x);
    }

    //Return true if the target position is in the same rank as the chess
    public boolean isSameRank(int y){return (this.getY() == y); }

    //Return true if there's any chess piece between current position and target position, only along any file or rank
    public boolean hasChessOnTheWay(int x, int y, Chessboard chessboard){
        if(this.isSameFile(x)){
            if (y > this.getY()){
                for (int i = this.getY() + 1; i < y; i ++){
                    if (chessboard.getChessByPosition(x, i) != null){return true;}
                }
            }
            else {
                for (int i = this.getY() - 1; i > y; i --){
                    if (chessboard.getChessByPosition(x, i) != null){return true;}
                }
            }
        }

        else if (this.isSameRank(y)){
            if (x > this.getX()){
                for (int i = this.getX() + 1; i < x; i++){
                    if (chessboard.getChessByPosition(i, y) != null){return true;}
                }
            }
            else {
                for (int i = this.getX() - 1; i > x; i--){
                    if (chessboard.getChessByPosition(i, y) != null){return true;}
                }
            }
        }
        return false;
    }

    //Return true if current position and target position is diagonal
    public boolean isDiagonally(int x, int y){
        return (Math.abs(this.x - x)  == Math.abs(this.y - y));
    }

    //Return true if the target position is in the topLeft of current position
    private boolean isTopLeft(int x, int y){
        return ((x - this.x) < 0 && (y - this.y) < 0);
    }

    //Return true if the target position is in the top right of current position
    private boolean isTopRight(int x, int y){
        return ((x - this.x) > 0 && (y - this.y) < 0);
    }

    //Return true if the target position is in the bottom left of current position
    private boolean isBottomLeft(int x, int y){
        return ((x - this.x) < 0 && (y - this.y) > 0);
    }

    //Return true if the target position is in the bottom right of current position
    private boolean isBottomRight(int x, int y){
        return ((x - this.x) > 0 && (y - this.y) > 0);
    }


    //Return true if there's any chess piece between current position and target position, only diagonally
    public boolean hasChessOnTheWayDiagonally(int x, int y, Chessboard chessboard) {
        int h = Math.abs(this.x - x);
        if (isTopLeft(x, y)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getChessByPosition((this.x -i), (this.y - i)) != null) {return true; }
            }
        } else if (isTopRight(x, y)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getChessByPosition((this.x + i), (this.y - i)) != null) { return true; }
            }
        } else if (isBottomLeft(x, y)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getChessByPosition((this.x - i), (this.y + i)) != null) { return true; }
            }
        } else if (isBottomRight(x, y)) {
            for (int i = 1; i < h; i++) {
                if (chessboard.getChessByPosition((this.x + i), (this.y + i)) != null) { return true; }
            }
        }
        return false;
    }

    //Return true if the target position are current position is L shape
    public boolean isLShape(int x, int y){
        if(Math.abs(this.y - y) == 2){return (Math.abs(this.x-x) == 1);}
        else if(Math.abs(this.y - y) == 1){return (Math.abs(this.x-x) == 2);}
        return false;
    }

    //Return true if the target position is in one square to the current position
    public boolean isInOneSquare(int x, int y){
        //same file
        if (this.x == x){return (Math.abs(y - this.y) <= 1);}
        //same rank
        if (this.y == y){return (Math.abs(x - this.x) <= 1);}
        //diagonals
        return ((Math.abs(y - this.y) == 1) && (Math.abs(x - this.x) == 1));
    }

    //Return true if the player has black chess pieces
    private boolean isBlackPlayer(){
        return (this.player == 2);
    }

    //Return true if there's mo chess in the target position
    public boolean isUnoccupied(int x , int y, Chessboard chessboard){
        return (chessboard.getChessByPosition(x, y) == null);
    }

    //Return true if the target position is immediately in front of the current position
    public boolean isInFrontOfImmediately(int x, int y){
        if (this.isBlackPlayer()){return ((y - this.y) == 1) && this.x  == x;}
        else {return ((this.y - y) == 1) && this.x == x;}
    }

    //Return true if there's no chess in the two grids immediately in front of the current position
    public boolean isTwoUnoccupied(int x, int y, Chessboard chessboard){
        if (this.isBlackPlayer()){
            boolean is2FrontImmediately = (y - this.y) == 2;
            boolean oneNoChess = this.isUnoccupied(x, (this.y+1), chessboard);
            boolean twoNoChess = this.isUnoccupied(x, (this.y+2), chessboard);
            return is2FrontImmediately && oneNoChess && twoNoChess;
        }
        else{
            boolean is2FrontImmediately = (this.y - y) == 2;
            boolean oneNoChess = this.isUnoccupied(x, this.y-1, chessboard);
            boolean twoNoChess = this.isUnoccupied(x, this.y-2, chessboard);
            return is2FrontImmediately && oneNoChess && twoNoChess;
        }
    }

    //Return true if the chess on the target position is opponent's
    public boolean isOpponent(int x, int y, Chessboard chessboard){
        Chess chess = chessboard.getChessByPosition(x, y);
        if (chess == null) return false;
        return (this.getPlayer() != chess.getPlayer());
    }

    //Return true if the target is in front of the current position and is on the diagonal
    public boolean isDiagonallyInFront(int x, int y){
        if(this.isBlackPlayer()){ return this.isDiagonally(x, y) && (this.y - y) > 0; }
        else{ return this.isDiagonally(x, y) && (y - this.y) < 0; }
    }

    //Return true if the target position is in the adjacent file of the current position
    public boolean isAdjacentFile(int x){
        return Math.abs(x - this.x) == 1;
    }

    //The current chess captures the target chess piece
    public void capture(int x, int y, Chessboard chessboard){
        Chess chess = chessboard.getChessByPosition(x, y);
        chess.isAlive = false;
        return;
    }

    //Return true if the chess is king
    public boolean isKing(){
        if (this.getSymbol() == '♚' || this.getSymbol()  == '♔'){return true;}
        return  false;
    }

}

