package ChessGame;
import pieces.Piece;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Chessboard {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    private int width;
    private int length;
    private Piece[][] board;
    private Player player1; //white
    private Player player2; //black
    private int currentPlayerColor;
    private HashMap map;


    //setters and getters
    public int getLength() {return length;}
    public int getWidth() {return width;}
    public Piece[][] getBoard(){return board;}
    public int getCurrentPlayerColor(){return currentPlayerColor;}
    public Player getPlayer1(){return this.player1;}
    public Player getPlayer2(){return this.player2;}
    public HashMap getMap(){return this.map;}

    public void setLength(int length) {this.length = length; }
    public void setWidth(int width) {this.width = width; }
    public void setPlayer1(int color){this.player1 = new Player(color);} //player1 is white
    public void setPlayer2(int color){this.player2 = new Player(color);}  //player2 is black
    public void setCurrentPlayerColor(int color){this.currentPlayerColor = color;}
    public void setBoard(int x, int y, Piece piece){
        board[y][x] = piece;
        int[] xy = {x, y};
        if(piece != null) this.map.put(piece, xy);
    }

    //constructor
    public Chessboard(int width, int length) {
        this.setLength(length);
        this.setWidth(width);
        this.setPlayer1(Piece.WHITE);
        this.setPlayer2(Piece.BLACK);
        this.board = new Piece[this.width][this.length];
        this.map = new HashMap<Piece, int[]>();
    }

    /**
     * Set the board to the initial status
     */
    public void setTheBoard(){
        Piece[] whitePieces = Utils.generateNewPieces(Piece.WHITE);
        Piece[] blackPieces = Utils.generateNewPieces(Piece.BLACK);
        this.setThePieceForAPlayer(blackPieces, Piece.BLACK);
        this.setThePieceForAPlayer(whitePieces, Piece.WHITE);
    }

    /**
     * Set one player's pieces to initial position
     * @param pieces The pieces for a player needed to set in the right position on the chessboard
     * @param color The color of the pieces
     */
    public void setThePieceForAPlayer(Piece[] pieces, int color){
        int y0, y1;
        if(color == Piece.BLACK){
            y0 = 0;
            y1 = 1;
        }
        else{
            y0 = 7;
            y1 = 6;
        }
        this.setBoard(0, y0, pieces[6]);
        this.setBoard(7, y0, pieces[7]);
        this.setBoard(1, y0, pieces[4]);
        this.setBoard(6, y0, pieces[5]);
        this.setBoard(2, y0, pieces[2]);
        this.setBoard(5, y0, pieces[3]);
        this.setBoard(4, y0, pieces[1]);
        this.setBoard(3, y0, pieces[0]);
        for (int i = 0; i < 8; i++){
            this.setBoard(i, y1, pieces[i + 8]);
        }
        this.generatePiecePositionMap();
    }

    /**
     * Generate Piece position(x, y) map to trace down each piece's location
     */
    public void generatePiecePositionMap(){
        for(int i = 0; i < this.length; i++){
            for (int j = 0; j < this.width; j ++){
                if (this.board[i][j] == null){continue;}
                int[] xy = new int[2];
                xy[0] = j;
                xy[1] = i;
                this.map.put(this.board[i][j], xy);
            }
        }
    }


    /**
     * Return piece by piece position
     * @param x The x coordinate of the piece
     * @param y The y coordinate of the piece
     * @return The Piece object at position (x, y)
     */
    public Piece getPieceByPosition(int x, int y){
        if (x >= this.width || y >= this.length || x < 0 || y < 0){
            Format.outOfBound();
            return null;
        }
        return this.board[y][x];
    }

    /**
     * Show the Chessboard
     * @param x The x coordinate of the selected piece
     * @param y The y coordinate of the selected piece
     */
    public void showChessboard(int x, int y){
        Format.printBanner(this.currentPlayerColor);
        System.out.print("   ");
        for (char i = 97; (i-97) < this.width; i ++){
            System.out.print(i);
            System.out.print(' ');
        }
        System.out.println();
        System.out.print(' ');
        for (int i = 0; i < (this.width * 2 + 3);i++){
            System.out.print('-');
        }

        System.out.println();
        for (int i = 0; i < length; i++){
            System.out.print(8-i);
            System.out.print('|');
            for (int j = 0; j < width; j++){
                System.out.print(' ');
                Piece c = this.getBoard()[i][j];
                if (c == null || c.getIsAlive() == false){
                    if (i == y && j == x){System.out.print(ANSI_RED + 'o' + ANSI_RESET);}
                    else System.out.print('o');
                }
                else {
                    if (i == y && j == x){System.out.print(ANSI_RED + c.getSymbol() + ANSI_RESET);}
                    else {System.out.print(c.getSymbol());}
                }
            }
            System.out.print(" |");
            System.out.println();
        }
        System.out.print(' ');
        for (int i = 0; i < (this.width * 2 + 3);i++){
            System.out.print('-');
        }
        System.out.println();
    }

    /**
     * Return true if position is out of chessboard
     * @param x
     * @param y
     * @return True if position is out of chessboard
     */
    public boolean positionOutOfBound(int x, int y){
        if (x >= this.width || y >= this.length || x < 0 || y < 0){
            Format.outOfBound();
            return true;
        }
        return false;
    }

    /**
     * Return true if this piece at position (x, y) is in the same color as the current player
     * @param x The x coordinate of piece
     * @param y The y coordinate of piece
     * @return True if this piece at position (x, y) is in the same color as the current player
     */
    public boolean hasSameColor(int x, int y){
        Piece piece = this.getPieceByPosition(x, y);
        if(piece == null)return false;
        if(piece.getColor() == this.currentPlayerColor){return true;}
        return false;
    }

    /**
     * Get the player by a specific color
     * @param color The color of the player
     * @return The player in the specific color
     */
    public Player getPlayerByColor(int color){
        if(color == player1.getColor())return player1;
        else return player2;
    }

    /**
     * Change the turn
     */
    public void flipCurrentPlayer(){
        if (this.currentPlayerColor == Player.WHITE){this.currentPlayerColor = Player.BLACK;}
        else {this.currentPlayerColor = Player.BLACK;}
    }

    /**
     * Get xy coordinates by piece
     * @param piece The key of the map
     * @return int array. array[0] is x coordinate, array[1] is y coordinate
     */
    public int[] getCoordinatesByPiece(Piece piece){
        if(piece == null) return null;
        return (int[]) this.map.get(piece);
    }

    /**
     * Return true if there's mo chess in the target position
     * @param x The x coordinate of the target position
     * @param y The y coordinate of the target position
     * @return Return true if there's mo chess in the target position
     */
    public  boolean isUnoccupied(int x , int y){
        return (this.getPieceByPosition(x, y) == null);
    }


    /**
     * Return true if current player's opponent's king is dead
     * @param currentPlayerColor Current player's color
     * @return Return true if current player's opponent's king is dead
     */
    public boolean isOpponentKingDead(int currentPlayerColor){
        Piece king = this.getKingByColor(this.getOpponentsColor(currentPlayerColor));
        if(king.getIsAlive())return false;
        else return true;
    }

    /**
     * Return the color of the current player's opponent
     * @param currentPlayerColor Current player's color
     * @return Return the color of the current player's opponent
     */
    public int getOpponentsColor(int currentPlayerColor){
        if (currentPlayerColor == Player.BLACK){return Player.WHITE;}
        else return Player.BLACK;
    }

    /**
     * Get Piece by type and color
     * @param type The piece's type
     * @param color the piece's color
     * @return The piece in the specific type and color
     */
    public Piece getPieceByTypeAndColor(int type, int color){
        Iterator it = this.map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Piece piece = (Piece)pair.getKey();
            if(piece.getType() == type && piece.getColor() == color){return piece;}
        }
        return null;
    }

    /**
     * Get King by type and color
     * @param color The king's color
     * @return The King in color
     */
    public Piece getKingByColor(int color){
        return this.getPieceByTypeAndColor(Piece.KING, color);
    }

    /**
     * Return true if the game ends because of one's king's dead or there are only two kings left on the chessboard
     * @param currentPlayerColor The current player's color
     * @return True if the game ends because of one's king's dead or there are only two kings left on the chessboard
     */
    public boolean checkEnd(int currentPlayerColor){
        if(this.isOpponentKingDead(currentPlayerColor)){
            Format.printWinner(currentPlayerColor);
            return true;
        }
        if(this.player1.getNumberOfAlivePieces() == 1 && this.player2.getNumberOfAlivePieces() == 1){
            Format.printWinner(-1);
            return true;
        }
        return false;
    }

    //Return true if the current player's king is under check, given current player number
    public boolean isUnderCheck(int currentPlayerColor) {
        Piece king = this.getPieceByTypeAndColor(Piece.KING, currentPlayerColor);
        int[] xy = this.getCoordinatesByPiece(king);
        Iterator it = this.map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Piece piece = (Piece)pair.getKey();
            if(piece.getColor() != king.getColor() && piece.canMove(xy[0], xy[1], this)){return true;}
        }
        return false;
    }

    public boolean hasOnlyKingLeft(int currentPlayerColor){
        Iterator it = this.map.entrySet().iterator();
        int sum = 0;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Piece piece = (Piece)pair.getKey();
            if(piece.getColor() == currentPlayerColor){sum++;}
        }
        return sum == 1;
    }






//    public Piece[] getAllAlivePiecesByColor(int color){
//        int len = 0;
//        Piece[] pieces = new Piece[Player.NUMBEROFPIECES];
//        Iterator it = this.map.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry pair = (Map.Entry)it.next();
//            Piece piece = (Piece)pair.getKey();
//            if(piece.getColor() == color){
//                pieces[len] = piece;
//                len++;
//            }
//        }
//        Piece[] allPieces = new Piece[len];
//        for(int i = 0; i < len; i++){
//            allPieces[i] = pieces[i];
//        }
//        return allPieces;
//    }

    //Return true if the current player is under stale mate
    public boolean isStaleMate(int currentPlayerColor){
        Piece king = getPieceByTypeAndColor(Piece.KING, currentPlayerColor);
        if(!this.isUnderCheck(currentPlayerColor)){
//            Piece[] allPieces = this.getAllAlivePiecesByColor(getOpponentsColor(currentPlayerColor));
            int[][] potentialPositions = this.getPotentialPositions(king);
            for(int i = 0; i < potentialPositions.length; i++){
                int[] xy = getCoordinatesByPiece(king);
                this.setBoard(potentialPositions[i][0], potentialPositions[i][1], king);
                this.setBoard(xy[0], xy[1], null);
                if(!isUnderCheck(currentPlayerColor)){return false;}
                this.setBoard(xy[0], xy[1], king);
                this.setBoard(potentialPositions[i][0], potentialPositions[i][1], null);
            }
            return true;
        }
        return false;
    }

    public int[][] getPotentialPositions(Piece piece){
        int[][] positions = new int[64][2];
        int index = 0;
        for(int i = 0; i < this.getLength(); i++){
            for(int j = 0; j < this.getWidth(); j++){
                if(piece.canMove(j, i, this)){
                    positions[index][0] = j;
                    positions[index][1] = i;
                    index++;
                }
            }
        }
        int [][]potentialPositions = new int[index+1][2];
        for(int i = 0; i <=index; i++){
            potentialPositions[i] = positions[i];
        }
        return potentialPositions;
    }

//    //Return the potential position where the king could move next step
//    public int[][] getKingPotentialPositions(int x, int y){
//        int length = 0; //length of return array
//        //all the possible squares where king could move
//        int [][] potentials = {
//                {x-1, y-1},
//                {x, y-1},
//                {x+1, y-1},
//                {x-1, y},
//                {x+1, y},
//                {x-1, y+1},
//                {x, y+1},
//                {x+1, y+1}
//        };
//
//        //filter invalid move
//        for (int i = 0; i < 8; i++){
//            if (this.getPieceByPosition(x, y).canMove(potentials[i][0], potentials[i][1], this)){
//                length ++;
//            }
//            else{
//                potentials[i][0] = -1;
//                potentials[i][1] = -1;
//            }
//        }
//
//        int [][] ret = new int[length][2];
//        int index = 0;
//        for (int i = 0; i < 8; i++) {
//              if (potentials[i][0] != -1){
//                  ret[index][0] = potentials[i][0];
//                  ret[index][1] = potentials[i][1];
//              }
//        }
//        return ret;
//    }
//
//    public boolean isCheckMate(int currentPlayerNumber) {
//        if (this.isUnderCheck(currentPlayerNumber)!=null) {
//            int []threatPosition = this.isUnderCheck(currentPlayerNumber);
//            Piece piece = this.getPlayerbyPlayerNumber(currentPlayerNumber).getKing();
//            int[][] potentialPositions = this.getKingPotentialPositions(piece.getX(), piece.getY());
//            for (int i = 0; i < potentialPositions.length; i++) {
//                if (!this.isUnderCheckByPosition(potentialPositions[i][0], potentialPositions[i][0],
//                        this, currentPlayerNumber)) {
//                    return false;
//                }
//            }
//        }
//        return false;
//    }






}
