package ChessGame;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

public class Piece {
    // Colors
    public static int WHITE = 0;
    public static int BLACK = 1;

    // Types
    public static int ROOK = 0;
    public static int KNIGHT = 1;
    public static int BISHOP = 2;
    public static int PAWN = 3;
    public static int QUEEN = 4;
    public static int KING = 5;

    private int color;
    private int type;
    private boolean isAlive;
    private char symbol = ' ';


    // getters and setters
    public int getColor() {return this.color;}
    public int getType() {return this.type;}
    public boolean getIsAlive(){return this.isAlive;}
    public char getSymbol(){return this.symbol;}

    public void setColor(int color){this.color = color;}
    public void setType(int type){this.type = type;}
    public void setIsAlive (boolean isAlive){this.isAlive = isAlive;}
    public void setSymbol(char symbol){this.symbol = symbol;}

    /**
     * Return true if the current chess piece is currentPlayer's and is alive
     * @param piece The current piece
     * @param currentPlayerColor The current player's color
     * @return Return true if the current chess piece is currentPlayer's and is alive
     */
    public static boolean isValidPiece(Piece piece, int currentPlayerColor){
        //check null
        if(piece == null) {
            Format.invalidPosition();
            return false;
        }
        //check is_existent and is_alive
        if (!piece.getIsAlive()) {
            Format.doesNotExist();
            return false;
        }
        //check this chess is the current player's
        if (piece.getColor() != currentPlayerColor){
            Format.notYourPiece();
            return false;
        }
        return true;
    }


    /**
     * Return true if it's valid to move to the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard
     * @return
     */
    public boolean canMove(int x, int y, Chessboard chessboard){return false;}

    /**
     * Move the piece to the target position. If there is any piece on the target position, capture it.
     * @param x
     * @param y
     * @param chessboard
     */
    public void moveAndCapture(int x, int y, Chessboard chessboard){
        Piece piece = chessboard.getPieceByPosition(x, y);
        if (piece != null){this.capture(x, y, chessboard);}
        this.move(x, y, chessboard);
    }

    /**
     * Move the piece to the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard where the piece moves
     */
    public void move(int x, int y, Chessboard chessboard){

        int[] xy = chessboard.getCoordinatesByPiece(this);
        chessboard.setBoard(x, y, this);
        chessboard.setBoard(xy[0], xy[1], null);
        xy[0] = x;
        xy[1] = y;
        chessboard.getMap().put(this, xy);
    }

    /**
     * Kill the piece on the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard where the piece moves
     */
    public void capture(int x, int y, Chessboard chessboard){
        Piece tgtPiece = chessboard.getPieceByPosition(x, y);
        tgtPiece.setIsAlive(false);
        chessboard.getMap().remove(tgtPiece);
        chessboard.setBoard(x, y, null);
        Player opponent = chessboard.getPlayerByColor(tgtPiece.color);
        opponent.decreaseNumberOfPieces();
    }

    /**
     * Return true if the chess on the target position is opponent's
     * @param piece THe piece on the target position
     * @return Return true if the chess on the target position is opponent's
     */
    public boolean isOpponent(Piece piece){
        if (this.getColor() == piece.getColor()){return false;}
        else return true;
    }

    /**
     * @return True if the chess is king
     */
    public boolean isKing(){
        return this.getType() == Piece.KING;
    }

}

