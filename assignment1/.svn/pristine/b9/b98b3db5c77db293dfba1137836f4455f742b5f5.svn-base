package tests;

import ChessGame.Chessboard;
import ChessGame.King;
import ChessGame.Piece;
import ChessGame.Utils;
import junit.framework.TestCase;

public class UtilsTest extends TestCase {
    Chessboard chessboard = new Chessboard(8,8);

    public void testConvertInput2Coordinates() throws Exception {
        assertEquals(5, Utils.convertInput2Coordinates("f2")[0]);
        assertEquals(6, Utils.convertInput2Coordinates("f2")[1]);
    }

    public void testIsDiagonally() throws Exception {
        assertEquals(true, Utils.isDiagonally(0, 6, 6, 0));
        assertEquals(true, Utils.isDiagonally(0, 6, 1, 7));
        assertEquals(false, Utils.isDiagonally(0, 6, 1, 6));
    }

    public void testIsTopLeft() throws Exception {
        assertEquals(true, Utils.isTopLeft(1, 1, 0, 0));
        assertEquals(false, Utils.isTopLeft(1, 1, 1, 0));
    }

    public void testIsTopRight() throws Exception {
        assertEquals(true, Utils.isTopRight(1, 1, 2, 0));
        assertEquals(false, Utils.isTopRight(1, 1, 0, 0));
    }

    public void testIsBottomLeft() throws Exception {
        assertEquals(true, Utils.isBottomLeft(1, 1, 0, 3));
        assertEquals(false, Utils.isBottomLeft(1, 1, 0, 0));
    }

    public void testIsBottomRight() throws Exception {
        assertEquals(true, Utils.isBottomRight(1, 1, 2, 2));
        assertEquals(false, Utils.isBottomRight(1, 1, 0, 0));
    }

    public void testIsInOneSquare() throws Exception {
        assertEquals(true, Utils.isInOneSquare(1, 1, 0, 0));
        assertEquals(false, Utils.isInOneSquare(1, 1, 3, 3));
    }

    public void testIsLShape() throws Exception {
        assertEquals(true, Utils.isLShape(1, 1, 3, 0));
        assertEquals(false, Utils.isLShape(1, 1,1, 0));
    }

    public void testIsInFrontOfImmediately() throws Exception {
        Piece piece = new King(Piece.BLACK);
        assertEquals(true, Utils.isInFrontOfImmediately(1, 1, 1, 2, piece));
        piece = new King(Piece.WHITE);
        assertEquals(false, Utils.isInFrontOfImmediately(1, 1, 1, 2, piece));
    }

    public void testIsDiagonallyInFront() throws Exception {
        Piece piece = new King(Piece.BLACK);
        assertEquals(true, Utils.isDiagonallyInFront(1, 1, 0, 2, piece));
        piece = new King(Piece.WHITE);
        assertEquals(false, Utils.isDiagonallyInFront(1, 1, 0, 2, piece));
    }

    public void testHasChessOnTheWayDiagonally() throws Exception {
        chessboard.setTheBoard();
        assertEquals(true, Utils.hasChessOnTheWayDiagonally(0, 7,7, 0, chessboard));
        assertEquals(false, Utils.hasChessOnTheWayDiagonally(7, 6, 2, 0, chessboard));
    }

    public void testIsTwoUnoccupied() throws Exception {
        chessboard.setTheBoard();
        Piece piece = chessboard.getPieceByPosition(0, 7);
        assertEquals(false, Utils.isTwoUnoccupied(0, 7, 0, 5, chessboard, piece));
        piece = chessboard.getPieceByPosition(0, 6);
        assertEquals(true, Utils.isTwoUnoccupied(0, 6, 0, 4, chessboard, piece));
        piece = chessboard.getPieceByPosition(0, 0);
        assertEquals(false, Utils.isTwoUnoccupied(0, 0, 7, 7, chessboard, piece));
        assertEquals(false, Utils.isTwoUnoccupied(0, 0, 2, 0, chessboard, piece));
        assertEquals(true, Utils.isTwoUnoccupied(0, 1, 0, 3, chessboard, piece));
    }

    public void testHasChessOnTheWay() throws Exception {
        chessboard.setTheBoard();
        assertEquals(true, Utils.hasChessOnTheWay(0, 6, 2, 6, chessboard));
        assertEquals(true, Utils.hasChessOnTheWay(0, 6, 0, 0, chessboard));
        assertEquals(true, Utils.hasChessOnTheWay(7, 6, 7, 0, chessboard));
        assertEquals(true, Utils.hasChessOnTheWay(5, 6, 7, 6, chessboard));
        assertEquals(false, Utils.hasChessOnTheWay(0, 6, 0, 1, chessboard));
        assertEquals(false, Utils.hasChessOnTheWay(0, 6,7, 0, chessboard));
    }

    public void testGenerateNewPieces() throws Exception {
        Piece[] pieces = Utils.generateNewPieces(Piece.WHITE);
        assertEquals(Piece.KING, pieces[0].getType());
        assertEquals(Piece.WHITE, pieces[0].getColor());
    }

    public void testIsAdjacentFile() throws Exception {
        chessboard.setTheBoard();
        assertEquals(true, Utils.isAdjacentFile(2, 1));
    }

    public void testIsSameRank() throws Exception {
        assertEquals(true, Utils.isSameRank(3, 3));
        assertEquals(false, Utils.isSameRank(2, 4));
    }

    public void testIsSameFile() throws Exception {
        assertEquals(true, Utils.isSameFile(1, 1));
        assertEquals(false, Utils.isSameFile(2, 3));
    }

}