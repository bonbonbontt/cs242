package ChessGame.pieces;

import ChessGame.Chessboard;
import ChessGame.Utils;

public class Hopper extends Piece {
    /**
     * Constructor
     * @param color The color of the piece
     */
    public Hopper(int color){
        this.setColor(color);
        this.setType(Piece.HOPPER);
        this.setIsAlive(true);
        if (color == Piece.BLACK){this.setSymbol('☗');}
        else{this.setSymbol('☖');}
    }

    /**
     * A Hopper can only move to the adjacent square or it can jump over another piece
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard where all ChessGame.pieces move
     * @return Return true if it's valid to move to the target position
     */
    @Override
    public boolean canMove(int x, int y, Chessboard chessboard) {
        int[] xy = chessboard.getCoordinatesByPiece(this);
        boolean jump = chessboard.isUnoccupied(x, y)
                && Utils.canJump(xy[0], xy[1], x, y, chessboard);
        boolean moveForward = chessboard.isUnoccupied(x, y) && Utils.isInOneSquare(xy[0], xy[1], x, y);

        if(jump || moveForward){
            return true;
        }
        return false;
    }
}
