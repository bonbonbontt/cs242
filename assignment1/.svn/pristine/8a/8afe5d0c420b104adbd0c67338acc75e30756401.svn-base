package ChessGame;

public class Pawn extends Piece {
    private boolean isFirstMove;
    /**
     * Constructor
     * @param color The color of the piece
     */
    public Pawn(int color){
        this.setColor(color);
        this.setType(Piece.PAWN);
        this.setIsAlive(true);
        this.isFirstMove = true;
        if (color == Piece.PAWN){this.setSymbol('♟');}
        else{this.setSymbol('♙');}
    }

    /**
     * Return true if it's this pawn's first move
     * @return True if it's this pawn's first move
     */
    private boolean isFirstMove(){return this.isFirstMove;}

    /**
     * Move the pawn to the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard where the pawn moves
     */
    @Override
    public void move(int x, int y, Chessboard chessboard) {
        if (this.isFirstMove){this.isFirstMove = false;}
        chessboard.setBoard(x, y, this);
    }

    /**
     * Return true if it's valid to move to the target position
     * @param x The target x coordinate
     * @param y The target y coordinate
     * @param chessboard The chessboard where all pieces move
     * @return Return true if it's valid to move to the target position
     */
    @Override
    public boolean canMove(int x, int y, Chessboard chessboard){
        int[] xy = chessboard.getCoordinatesByPiece(this);
        //move forward to the unoccupied square immediately in front of it on the same file
        if(chessboard.isUnoccupied(x, y) && Utils.isInFrontOfImmediately(xy[0], xy[1], x, y, this)){
            if (this.isFirstMove) this.isFirstMove = false;
            return true;
        }
        if(this.isFirstMove() && Utils.isSameFile(xy[0], x) &&
                Utils.isTwoUnoccupied(xy[0], xy[1], x, y, chessboard, chessboard.getPieceByPosition(xy[0], xy[1]))){

            if (this.isFirstMove) this.isFirstMove = false;
            return true;
        }
        if(!chessboard.isUnoccupied(x, y)
                && Utils.isDiagonallyInFront(xy[0], xy[1], x, y, chessboard.getPieceByPosition(x, y))
                && Utils.isAdjacentFile(xy[0], x) && this.isOpponent(chessboard.getPieceByPosition(x, y))){
            if (this.isFirstMove) this.isFirstMove = false;
            return true;
        }
        return false;
    }
}
