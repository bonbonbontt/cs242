package tests;

import ChessGame.*;
import junit.framework.TestCase;

import static ChessGame.Piece.WHITE;

public class BishopTest extends TestCase {
    public Chessboard chessboard = new Chessboard(8, 8);

    //Test constructor
    public void testConstructor(){
        Piece piece = new Bishop(WHITE);
        assertEquals(WHITE, piece.getColor());
        assertEquals(Piece.BISHOP, piece.getType());
        assertEquals('♗', piece.getSymbol());
        assertEquals(true, piece.getIsAlive());
    }

    public void testMoveTopLeft() {
        Piece piece = chessboard.getPieceByPosition(2, 7);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(2, 2, chessboard));
        assertEquals(piece, chessboard.getPieceByPosition(2, 2));
    }

    public void testMoveTopRight() {
        Piece piece = chessboard.getPieceByPosition(2, 7);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(4 , 2, chessboard));
        assertEquals(piece, chessboard.getPieceByPosition(4, 2));
    }

    public void testMoveBottomLeft() {
        Piece piece = chessboard.getPieceByPosition(2, 7);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(2 , 4, chessboard));
        assertEquals(piece, chessboard.getPieceByPosition(2, 4));
    }

    public void testMoveBottomRight() {
        Piece piece = chessboard.getPieceByPosition(2, 7);
        chessboard.setBoard(3, 3, piece);
        assertEquals(true, piece.canMove(5, 5, chessboard));
        assertEquals(piece, chessboard.getPieceByPosition(5, 5));
    }

    public void testMoveNotDiagonally(){
        Piece piece = chessboard.getPieceByPosition(2, 7);
        chessboard.setBoard(3, 3, piece);
        assertEquals(false, piece.canMove(3, 5, chessboard));
        assertEquals(null, chessboard.getPieceByPosition(3, 5));
    }

    public void testLeapOver(){
        Piece piece = chessboard.getPieceByPosition(2, 7);
        chessboard.setBoard(3, 3, piece);

        Piece piece1 = chessboard.getPieceByPosition(0, 1);
        chessboard.setBoard(2, 4, piece1);
        assertEquals(piece1, chessboard.getPieceByPosition(2, 4));

        assertEquals(false, piece.canMove(1, 5, chessboard));
        assertEquals(null, chessboard.getPieceByPosition(1, 5));
    }

}