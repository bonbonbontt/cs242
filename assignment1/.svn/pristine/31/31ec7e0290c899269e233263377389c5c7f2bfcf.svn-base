package ChessGame.tests;

import ChessGame.*;
import ChessGame.pieces.*;
import junit.framework.TestCase;

public class ChessboardTest extends TestCase {
    Chessboard chessboard = new Chessboard(8,8);

    //Test chessboard and player constructor
    public void testConstructor (){
        assertEquals("chessboard length", 8, chessboard.getLength());
        assertEquals("chessboard width", 8, chessboard.getWidth());
        assertEquals("player1's color", Player.WHITE, chessboard.getPlayer1().getColor());
        assertEquals("player2's color", Player.BLACK, chessboard.getPlayer2().getColor());
    }

    public void testGeneratePiecePositionMap(){
        Piece piece  = new Bishop(Piece.WHITE);
        chessboard.setBoard(3, 3, piece);
        chessboard.generatePiecePositionMap();
        int[] xy = (int[]) chessboard.getMap().get(piece);
        assertEquals(3, xy[0]);
        assertEquals(3, xy[1]);
    }

    public void testSetThePieceForAPlayer(){
        Piece[] pieces = Utils.generateNewPieces(Piece.BLACK);
        chessboard.setThePieceForAPlayer(pieces, Piece.BLACK);
        assertEquals(Piece.ROOK, chessboard.getPieceByPosition(0,0).getType());
        assertEquals(Piece.BLACK, chessboard.getPieceByPosition(0,0).getColor());
    }

    public void testSetTheBoard(){
        chessboard.setTheBoard();
        Piece piece = chessboard.getPieceByPosition(0, 0);
        assertEquals(Piece.ROOK, chessboard.getPieceByPosition(0,0).getType());
        assertEquals(Piece.BLACK, chessboard.getPieceByPosition(0,0).getColor());

    }

    public void testShowChessboard(){

    }

    public void testHasSameColor(){
        chessboard.setTheBoard();
        assertEquals(true, chessboard.hasSameColor(7, 7));
        assertEquals(false, chessboard.hasSameColor(0, 0));
    }

    public void testGetPlayerColor(){
        chessboard.setTheBoard();
        assertEquals(chessboard.getPlayer1(), chessboard.getPlayerByColor(Player.WHITE));
    }

    public void testFlipCurretnPlayer(){
        chessboard.setTheBoard();
        assertEquals(Player.WHITE, chessboard.getCurrentPlayerColor());
        chessboard.flipCurrentPlayer();
        assertEquals(Player.BLACK, chessboard.getCurrentPlayerColor());
    }

    public void testGetCoordinatesByPiece(){
        chessboard.setTheBoard();
        Piece piece = chessboard.getPieceByPosition(3 ,1);
        int[] xy = chessboard.getCoordinatesByPiece(piece);
        assertEquals(3, xy[0]);
        assertEquals(1, xy[1]);
    }

    public void testIsUnoccupied(){
        chessboard.setTheBoard();
        assertEquals(true, chessboard.isUnoccupied(3, 3));
        assertEquals(false, chessboard.isUnoccupied(0, 0));
    }

    public void testGetOpponentsColor(){
        chessboard.setTheBoard();
        assertEquals(Piece.WHITE, chessboard.getOpponentsColor(Player.BLACK));
    }

    public void testGetPieceByTypeAndColor(){
        chessboard.setTheBoard();
        Piece piece = chessboard.getPieceByPosition(3, 0);
        assertEquals(piece, chessboard.getPieceByTypeAndColor(Piece.QUEEN, Piece.BLACK));

    }

    public void testGetKingByColor(){
        chessboard.setTheBoard();
        Piece piece = chessboard.getPieceByPosition(4, 0);
        assertEquals(piece, chessboard.getKingByColor(Piece.BLACK));
    }

    public void testCheckEnd(){
        //only kings left
        Piece blackKing = new King(Piece.BLACK);
        Piece whiteKing = new King(Piece.WHITE);
        chessboard.setBoard(0, 0, blackKing);
        chessboard.setBoard(1, 0, whiteKing);
        Player player1 = chessboard.getPlayerByColor(Player.WHITE);
        Player player2 = chessboard.getPlayerByColor(Player.BLACK);
        player1.setNumberOfAlivePieces(1);
        player2.setNumberOfAlivePieces(1);
        assertEquals(true, chessboard.checkEnd(Piece.WHITE));

        //king dead
        Piece piece = new Rook(Piece.BLACK);
        blackKing.setIsAlive(false);
        assertEquals(true, chessboard.checkEnd(Piece.WHITE));
    }

    public void testPositionOutOfBound(){
        assertEquals(true, chessboard.positionOutOfBound(8, 9));
        assertEquals(false, chessboard.positionOutOfBound(1, 2));
    }

    public void testIsUnderCheck(){
        Piece whiteBishop = new Bishop(Piece.WHITE);
        Piece blackKing = new King(Piece.BLACK);
        chessboard.setBoard(4, 0, blackKing);
        chessboard.setBoard(1, 3, whiteBishop);
        assertEquals(whiteBishop, chessboard.isUnderCheck(Piece.BLACK));
        chessboard.setCurrentPlayerColor(Piece.BLACK);
        chessboard.showChessboard(-1, -1);
    }

    public void testHasOnlyKingLeft(){
        Piece whiteKing = new King(Piece.WHITE);
        chessboard.setBoard(4, 0, whiteKing);
        assertEquals(true, chessboard.hasOnlyKingLeft(Piece.WHITE));
        Piece whiteBishop = new Bishop(Piece.WHITE);
        chessboard.setBoard(1, 3, whiteBishop);
        assertEquals(false, chessboard.hasOnlyKingLeft(Piece.WHITE));
    }

    public void testIsStaleMate(){
        Piece whiteKing = new King(Piece.WHITE);
        Piece blackKing = new King(Piece.BLACK);
        Piece whiteQueen = new Queen(Piece.WHITE);
        chessboard.setBoard(7, 0, blackKing);
        chessboard.setBoard(5, 1, whiteKing);
        chessboard.setBoard(6, 2, whiteQueen);
        chessboard.showChessboard(-1, -1);
        assertEquals(true, chessboard.isStaleMate(Player.BLACK));
    }

    public void testIsCheckMate(){
        chessboard.setTheBoard();
        chessboard.getPieceByPosition(3, 0).move(7, 4, chessboard);
        chessboard.getPieceByPosition(4, 1).move(4, 3, chessboard);
        chessboard.getPieceByPosition(5, 6).move(5, 5, chessboard);
        chessboard.getPieceByPosition(6, 6).move(6, 4, chessboard);
        chessboard.showChessboard(-1, -1);
        assertEquals(true, chessboard.isCheckMate(Player.WHITE));
    }

   

}