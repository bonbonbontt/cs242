package ChessGame;

import java.util.Scanner;

public class Chessboard {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    private int width;
    private int length;
    private Piece[][] board;
    private Player player1; //white
    private Player player2; //black
    private int currentPlayerColor;

    //setters and getters
    public int getLength() {return length;}
    public int getWidth() {return width;}
    public Piece[][] getBoard(){return board;}
    public int getCurrentPlayerColor(){return currentPlayerColor;}
    public Player getPlayer1(){return this.player1;}
    public Player getPlayer2(){return this.player2;}

    public void setLength(int length) {this.length = length; }
    public void setWidth(int width) {this.width = width; }
    public void setPlayer1(int color){this.player1 = new Player(color);} //player1 is white
    public void setPlayer2(int color){this.player2 = new Player(color);}  //player2 is black
    public void setCurrentPlayerColor(int color){this.currentPlayerColor = color;}
    public void setBoard(int x, int y, Piece piece){ board[y][x] = piece; }

    //constructor
    public Chessboard(int width, int length) {
        this.setLength(length);
        this.setWidth(width);
        this.setPlayer1(Piece.WHITE);
        this.setPlayer2(Piece.BLACK);
        this.board = new Piece[this.width][this.length];
    }

    //Set the board to the initial status
    public void setTheBoard(){
        Piece[] whitePieces = this.generateNewPieces(Piece.WHITE);
        Piece[] blackPieces = this.generateNewPieces(Piece.BLACK);
        this.setThePieceForAPlayer(blackPieces, Piece.BLACK);
        this.setThePieceForAPlayer(whitePieces, Piece.WHITE);
    }

    //Set one player's pieces to initial position
    public void setThePieceForAPlayer(Piece[] pieces, int color){
        int y0, y1;
        if(color == Piece.BLACK){
            y0 = 0;
            y1 = 1;
        }
        else{
            y0 = 7;
            y1 = 6;
        }
        this.setBoard(0, y0, pieces[6]);
        this.setBoard(7, y0, pieces[7]);
        this.setBoard(1, y0, pieces[4]);
        this.setBoard(6, y0, pieces[5]);
        this.setBoard(2, y0, pieces[2]);
        this.setBoard(5, y0, pieces[3]);
        this.setBoard(4, y0, pieces[1]);
        this.setBoard(3, y0, pieces[0]);
        for (int i = 0; i < 8; i++){
            this.setBoard(i, y1, pieces[i + 8]);
        }
    }

    //Return Piece array for a single player, given the color
    public Piece[] generateNewPieces(int color){
        Piece[] pieces = new Piece[Player.NUMBEROFPIECES];
        pieces[0] = new King(color);
        pieces[1] = new Queen(color);
        int index = 2;
        for (int i = 0; i < 2; i ++){
            pieces[index] = new Bishop(color);
            pieces[index + 2] = new Knight(color);
            pieces[index + 4] = new Rook(color);
            index ++;
        }
        for (int i = 0; i < 8; i++){
            pieces[i + 8] = new Pawn(color);
        }
        return pieces;
    }

    //Return chess by chess position
    public Piece getChessByPosition(int x, int y){
        if (x >= this.width || y >= this.length || x < 0 || y < 0){
            System.out.println("Error: out of bound!");
            return null;
        }
        return this.board[y][x];
    }

    public void printBanner(){
        String currentPlayer;
        if (this.currentPlayerColor == Player.WHITE){currentPlayer = "Player1";}
        else{currentPlayer = "Player2";}
        System.out.println("==================================");
        System.out.println("Player1: White      Player2: Black");
        System.out.println(currentPlayer + "'s move");
        System.out.println("==================================");
    }

    public void showChessboard(int x, int y){
        this.printBanner();
        System.out.print("   ");
        for (char i = 97; (i-97) < this.width; i ++){
            System.out.print(i);
            System.out.print(' ');
        }
        System.out.println();
        System.out.print(' ');
        for (int i = 0; i < (this.width * 2 + 3);i++){
            System.out.print('-');
        }

        System.out.println();
        for (int i = 0; i < length; i++){
            System.out.print(8-i);
            System.out.print('|');
            for (int j = 0; j < width; j++){
                System.out.print(' ');
                Piece c = this.getBoard()[i][j];
                if (c == null || c.getIsAlive() == false){System.out.print('o'); }
                else {
                    if (i == y && j == x){System.out.print(ANSI_RED + c.getSymbol() + ANSI_RESET);}
                    else {System.out.print(c.getSymbol());}
                }
            }
            System.out.print(" |");
            System.out.println();
        }
        System.out.print(' ');
        for (int i = 0; i < (this.width * 2 + 3);i++){
            System.out.print('-');
        }
        System.out.println();
    }

//    //Convert string input to coodinates stored in int array
//    public int[] convertInput2Coordinates(String input){
//        int y = 8 - Character.getNumericValue(input.charAt(1));
//        int x = input.charAt(0) - 97;
//        int[] ret = new int[2];
//        ret[0] = x;
//        ret[1] = y;
//        return ret;
//    }
//
//    //Return true if position is out of chessboard
//    public boolean positionOutOfBound(int x, int y){
//        if (x >= this.width || y >= this.length || x < 0 || y < 0){
//            System.out.println("Error: out of bound!");
//            return true;
//        }
//        return false;
//    }
//
//    //Change the turn
//    public void flipCurrentPlayer(){
//        if (this.currentPlayer == 1){this.currentPlayer = 2;}
//        else {this.currentPlayer = 1;}
//    }
//
//    //Return the winner's number if either king's dead, or return -1.
//    public int checkKingDead(){
//        Piece king1 = this.player1.getKing();
//        if (!king1.getIsAlive()) {
//            System.out.println("Player2 win!");
//            return 2;
//        }
//        Piece king2 = this.player2.getKing();
//        if (!king2.getIsAlive()) {
//            System.out.println("Player1 win!");
//            return 1;
//        }
//        return -1;
//    }
//
//    //Return true if both players have only king to move
//    public boolean onlyKings(){
//        return hasOnlyKing(1) && hasOnlyKing(2);
//    }
//
//    //Return true if the current player only has king to move
//    public boolean hasOnlyKing(int currentPlayerNumber){
//        if(currentPlayerNumber == 1){
//            Piece[] player1Pieces = this.player1.getAliveChess();
//            if (player1Pieces.length == 1 && player1Pieces[0].isKing()){return true;}
//        }
//        else {
//            Piece[] player2Pieces = this.player2.getAliveChess();
//            if (player2Pieces.length == 1 && player2Pieces[0].isKing()){return true;}
//        }
//        return false;
//    }
//
//    //Return opponent's player number
//    public int getOpponentPlayerNumber(int currentPlayerNumber){
//           if (currentPlayerNumber == 1){return 2;}
//           return 1;
//    }
//
//    //Return true if the current player's king is under check, given current player number
//    public int[] isUnderCheck(int currentPlayerNumber) {
//        Player opponent = this.getPlayerbyPlayerNumber(this.getOpponentPlayerNumber(currentPlayerNumber));
//        Piece[] opponentAlivePieces = opponent.getAliveChess();
//        Piece king = this.getPlayerbyPlayerNumber(currentPlayerNumber).getKing();
//        for (int i = 0; i < opponentAlivePieces.length; i++){
//             if (opponentAlivePieces[i].canMove(king.getX(), king.getY(), this)){
//                 int [] threat = {opponentAlivePieces[i].getX(), opponentAlivePieces[i].getY()};
//                 return threat;
//             }
//        }
//        return null;
//    }
//
//    //Return true if the potential position would make currentPlayer under check
//    public boolean isUnderCheckByPosition(int x, int y, Chessboard chessboard, int currentPlayerNumber){
//        Player opponent = this.getPlayerbyPlayerNumber(this.getOpponentPlayerNumber(currentPlayerNumber));
//        Piece[] opponentAlivePieces = opponent.getAliveChess();
//        for (int i = 0; i < opponentAlivePieces.length; i++){
//            if (opponentAlivePieces[i].canMove(x, y, this)){return true;}
//        }
//        return false;
//    }
//
//    //Return the corresponding player if given a currentPlayerNumber
//    public Player getPlayerbyPlayerNumber(int currentPlayerNumber){
//        if (currentPlayerNumber == 1) return this.getPlayer1();
//        else return this.getPlayer2();
//    }
//
//    //Return true if the current player is under stale mate
//    public boolean isStaleMate(int currentPlayerNumber){
//        if(this.hasOnlyKing(currentPlayerNumber) && this.isUnderCheck(currentPlayerNumber) == null){
//            return true;
//        }
//        return false;
//    }
//
//    //Return the potential position where the king could move next step
//    public int[][] getKingPotentialPositions(int x, int y){
//        int length = 0; //length of return array
//        //all the possible squares where king could move
//        int [][] potentials = {
//                {x-1, y-1},
//                {x, y-1},
//                {x+1, y-1},
//                {x-1, y},
//                {x+1, y},
//                {x-1, y+1},
//                {x, y+1},
//                {x+1, y+1}
//        };
//
//        //filter invalid move
//        for (int i = 0; i < 8; i++){
//            if (this.getChessByPosition(x, y).canMove(potentials[i][0], potentials[i][1], this)){
//                length ++;
//            }
//            else{
//                potentials[i][0] = -1;
//                potentials[i][1] = -1;
//            }
//        }
//
//        int [][] ret = new int[length][2];
//        int index = 0;
//        for (int i = 0; i < 8; i++) {
//              if (potentials[i][0] != -1){
//                  ret[index][0] = potentials[i][0];
//                  ret[index][1] = potentials[i][1];
//              }
//        }
//        return ret;
//    }
//
//    public boolean isCheckMate(int currentPlayerNumber) {
//        if (this.isUnderCheck(currentPlayerNumber)!=null) {
//            int []threatPosition = this.isUnderCheck(currentPlayerNumber);
//            Piece piece = this.getPlayerbyPlayerNumber(currentPlayerNumber).getKing();
//            int[][] potentialPositions = this.getKingPotentialPositions(piece.getX(), piece.getY());
//            for (int i = 0; i < potentialPositions.length; i++) {
//                if (!this.isUnderCheckByPosition(potentialPositions[i][0], potentialPositions[i][0],
//                        this, currentPlayerNumber)) {
//                    return false;
//                }
//            }
//        }
//        return false;
//    }
//
//    //Return the winner's player number. 0 if draws, -1 if not ends
//    public int checkEnd(int currentPlayer){
//        int ret = this.checkKingDead();
//        if (ret == -1){
//            if(this.onlyKings()){
//                System.out.println("Draw!");
//                return 0;
//            }
//            if(isStaleMate(currentPlayer)){
//                System.out.println("Draw!");
//                return 0;
//            }
//            if(isCheckMate(currentPlayer)){
//                if(currentPlayer == 1){
//                    System.out.println("Player2 win!");
//                    return 2;
//                }
//                else{
//                    System.out.println("Player1 win!");
//                    return 2;
//                }
//            }
//        }
//        return ret;
//    }




}
