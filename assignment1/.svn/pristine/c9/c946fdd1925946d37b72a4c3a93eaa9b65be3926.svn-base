package view;

import ChessGame.Chessboard;
import controller.Controller;

import java.awt.*;
import java.awt.event.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


public class View implements ActionListener{
    private JButton[][] buttons = new JButton[8][8];
    private JLabel[][] labels = new JLabel[2][4];
    private JPanel chessboardPanel;
    private JPanel infoPanel;
    private JFrame window;
    private JMenuItem restart;
    public static Color LIGHTBROWN = new Color(254, 206, 159);
    public static Color DARKBROWN = new Color(211, 139, 65);
    public static Color GOLD = new Color(70, 130, 180, 100);
    public static Color GREEN = new Color(154, 205, 50, 100);
    public static int WHITE = 0;
    public static int BLACK = 1;




    public JPanel getChessboardPanel() {
        return chessboardPanel;
    }

    public JPanel getInfoPanel() {
        return infoPanel;
    }

    public JMenuItem getRestart() {
        return restart;
    }

    public JButton[][] getButtons() {
        return buttons;
    }

    public JFrame getWindow() {
        return window;
    }

    public JLabel[][] getLabels(){return labels;}

    //constructor
    public View(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e) {
            //ignore
        }
        window = new JFrame("Chess");
        window.setSize(500, 600);
        this.chessboardPanel = new JPanel(new GridLayout(8, 8));
        this.infoPanel = new JPanel(new GridLayout(2, 8));
        //initialize chessboardPanel and piece buttons on it.
        this.initializeChessboardPanel();
//        initializeIcon(buttons);
        this.intializeInfoPanel();

        setUpMenu(window);
        window.getContentPane().add(infoPanel, BorderLayout.NORTH);
        window.getContentPane().add(chessboardPanel, BorderLayout.CENTER);
        window.setVisible(true);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void intializeInfoPanel(){
        this.infoPanel.setBackground(View.DARKBROWN);
        this.infoPanel.setBorder(new CompoundBorder(
                new EmptyBorder(8, 8, 8, 8),
                new LineBorder(Color.BLACK)
        ));
        for(int i = 0; i < this.labels.length; i++){
            for(int j = 0; j < this.labels[0].length; j++){
                JLabel l = new JLabel();
                if(i == 0){l.setBackground(View.LIGHTBROWN);}
                else{l.setBackground(View.DARKBROWN);}
//                l.setText(i + ", " + j);
                labels[i][j] = l;
                this.infoPanel.add(l);
            }

        }
        labels[0][0].setText("Player1:");
        labels[1][0].setText("Player2:");
        labels[0][2].setText("Score:");
        labels[1][2].setText("Score:");

    }

    public void setPlayerNameLabel(String name, int playerColor ){
        this.labels[playerColor][1].setText(name);
    }
    /**
     * Initialize the grid panel for chessboardPanel and set color
     */
    private void initializeChessboardPanel() {
        this.getChessboardPanel().setBorder(new CompoundBorder(
                new EmptyBorder(8, 8, 8, 8),
                new LineBorder(Color.BLACK)
        ));
        for (int i = 0; i < this.getButtons().length; i++) {
            for (int j = 0; j < this.getButtons()[0].length; j++) {
                JButton b = new JButton();
                if ((j % 2 == 1 && i % 2 == 1) || (j % 2 == 0 && i % 2 == 0)) {
                    b.setBackground(LIGHTBROWN);
                } else {
                    b.setBackground(DARKBROWN);
                }
                b.setOpaque(true);
                b.setBorder(null);
                this.getButtons()[j][i] = b;


                this.getChessboardPanel().add(this.getButtons()[j][i]);
            }
        }
    }

    public static void setAllBackgroundToOriginalColor(JButton[][] buttons){
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                JButton b = buttons[i][j];
                if ((j % 2 == 1 && i % 2 == 1)  || (j % 2 == 0 && i % 2 == 0)) {
                    b.setBackground(View.LIGHTBROWN);
                } else {
                    b.setBackground(DARKBROWN);
                }
            }
        }
    }

    /**
     * Add icon to each button
     * @param buttons All buttons array
     */
    private void initializeIcon(JButton[][] buttons) {
        String[] names = {
                "bking.png", "wking.png",
                "bq.png", "wq.png",
                "bb.png", "wb.png",
                "bk.png", "wk.png",
                "br.png", "wr.png",
                "bp.png", "wp.png"
        };
        Image[] images = new Image[12];

        for(int i = 0; i < names.length; i++){
            try {

                Image img = ImageIO.read(getClass().getResource(names[i]));
                images[i] = img.getScaledInstance( 50, 50,  java.awt.Image.SCALE_SMOOTH) ;

            } catch (Exception ex) {
            }
        }

        buttons[0][0].setIcon(new ImageIcon(images[8]));
        buttons[7][0].setIcon(new ImageIcon(images[8]));
        buttons[0][7].setIcon(new ImageIcon(images[9]));
        buttons[7][7].setIcon(new ImageIcon(images[9]));

        buttons[1][0].setIcon(new ImageIcon(images[6]));
        buttons[6][0].setIcon(new ImageIcon(images[6]));
        buttons[1][7].setIcon(new ImageIcon(images[7]));
        buttons[6][7].setIcon(new ImageIcon(images[7]));

        buttons[2][0].setIcon(new ImageIcon(images[4]));
        buttons[5][0].setIcon(new ImageIcon(images[4]));
        buttons[2][7].setIcon(new ImageIcon(images[5]));
        buttons[5][7].setIcon(new ImageIcon(images[5]));

        buttons[3][0].setIcon(new ImageIcon(images[0]));
        buttons[3][7].setIcon(new ImageIcon(images[1]));

        buttons[4][0].setIcon(new ImageIcon(images[2]));
        buttons[4][7].setIcon(new ImageIcon(images[3]));

        for(int i = 0; i < 8; i++){
            buttons[i][1].setIcon(new ImageIcon(images[10]));
            buttons[i][6].setIcon(new ImageIcon(images[11]));
        }

        for(int i = 0; i < buttons.length; i++){
            for(int j = 0; j < buttons[0].length; j++){
                buttons[i][j].setBorder(null);
            }
        }
    }

    /**
     * Set up the menu
     * @param window The window showing everything
     */
    private void setUpMenu(JFrame window) {
        JMenuBar menubar = new JMenuBar();
        this.restart = new JMenuItem("Start");

        JMenuItem undo = new JMenuItem("Undo");
        menubar.add(restart);
        menubar.add(undo);
        window.setJMenuBar(menubar);
    }


    public void actionPerformed(ActionEvent e) {}

//    public void reset(){
//        Controller.initializeChessboardPanel();
//        initializeIcon(buttons);
//    }



    public void showError(){}

    public static void main(String[] args) {
        new View();
    }

    public JButton getButtonByPosition(int x, int y){
        return this.buttons[x][y];
    }
}